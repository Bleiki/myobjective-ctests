//
//  AppDelegate.m
//  StringTest
//
//  Created by Admin on 02.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    NSString* string1 = @"String1";
    NSString* string2 = @"String2";

    NSArray* array = [NSArray arrayWithObjects:string1, string2, nil];
    
    for (NSString* string in array) {
        
        if ([string isEqualToString:@"String1"]) {
            
            NSLog(@"Index = %d", [array indexOfObject:string]);
        }
    }
    
    
    
    NSString* string = @"Hello World!";
    
    NSLog(@"%@", string);
    
    NSRange range = [string rangeOfString:@"World"];
    //NSRange range = [string rangeOfString:@"world" options:NSCaseInsensitiveSearch]; Найдет World, не смотря на то что мы ищем world с маленькой буквы.
    //NSRange range = [string rangeOfString:@"world" options:NSCaseInsensitiveSearch | NSBackwardsSearch]; Ищет в независимости от кейса буква и с конца строки (NSBackwardsSearch).
    
    if (range.location != NSNotFound) {
        
        NSLog(@"Range = %@", NSStringFromRange(range));

    } else {
        
        NSLog(@"Not found");

    }
    
    
    
    
    NSString* text = @" ";
    
    //text = [text ];
    
    NSRange range = [text rangeOfString:@"World"]; //Находим фразу
    
    if (range.location != NSNotFound) {
        
       text = [text substringToIndex:range.location];
    }


    NSLog(@"%@", text);
    

    //Нужно выполнять действия через указатель на строку, если строка не изменяемая.
    //substringFromIndex:(NSUInteger)from  Вырезает(оставляет) строку после индекса и до конца;
    //substringToIndex:(NSUInteger)to  Вырезает(оставляет) строку с начада и до индекса;
    //substringWithRange:(NSUInteger)range  Вырезает(оставляет) строку в интервале.
    //substringWithRange:NSMakeRange( , )  Вырезает(оставляет) строку в интервале.

    //Подсчет строк:
    
    NSString* text = @" ";
    
    NSRange searchRange = NSMakeRange(0, text.length);
    
    NSInteger count = 0;
    
    while(YES) {
        
        NSRange range = [text rangeOfString:@"NSString" options:0 range:searchRange];
        
        if (range.location != NSNotFound) {
            
            NSInteger index = range.location + range.length;
            
            searchRange.location = index;
            
            searchRange.length = [text length] - index;
            
            NSLog(@"%@", NSStringFromRange(range));

            count++;
            
        } else {
            break;
        }
    }
    
    NSLog(@"Count = %li", count);
    
    
    
    //Замена строки на другую:
    NSString* text = @" ";
    
    text = [text stringByReplacingOccurrencesOfString:@"NSString" withString:@"TRA-LA-LA"];
     */
    
    
    //Все пишется заглавными буквами:
    NSString* text = @" ";

    text = [text uppercaseString];
    
    
    //Все пишется строчными буквами:
    
    NSString* text = @" ";
    
    text = [text lowercaseString];
    
    
    //Все слова начинаются с большой буквы:
    
    NSString* text = @" ";
    
    text = [text capitalizedString];
    
    
    
    //Взять из текста компоненты (и добавить в массив), разделенный определенной строкой (в данном случае пробелом).
    
    NSString* text = @" ";

    NSArray* array = [text componentsSeparatedByString:@" "];

    NSLog(@"%@", array);

    text = [array componentsJoinedByString:@"_"]; //Соединение слов в массиве определенной строкой
    
     NSLog(@"%@", text);
    
    
    //Сложение строк(объединение):
    
    NSString* s1 = @"Hello ";

    NSString* s2 = @"World!";
    
    NSString* s3 = [s1 stringByAppendingString:s2];


    NSLog(@"%@ = %@%@", s3, s1, s2);
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
