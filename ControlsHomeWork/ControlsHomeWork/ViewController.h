//
//  ViewController.h
//  ControlsHomeWork
//
//  Created by Admin on 27.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISlider *firstSlider;
@property (weak, nonatomic) IBOutlet UIView *blueView;
@property (weak, nonatomic) IBOutlet UIView *brownView;
@property (weak, nonatomic) IBOutlet UIView *purpleView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *tripleSegment;
@property (weak, nonatomic) IBOutlet UISwitch *rotationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *translationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *scaleSwitch;


- (IBAction)actionTransformEnable:(UISwitch *)sender;
- (IBAction)actionValueChenge:(UISlider *)sender;
- (IBAction)actionSelectOfView:(UISegmentedControl *)sender;

@end

