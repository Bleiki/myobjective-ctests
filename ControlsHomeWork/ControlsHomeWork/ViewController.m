//
//  ViewController.m
//  ControlsHomeWork
//
//  Created by Admin on 27.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(strong, nonatomic) UIView* currentView;

@end

typedef enum {
    
    VGNumberOfViewOne,
    VGNumberOfViewTwo,
    VGNumberOfViewThree
    
}VGNumberOfView;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.currentView = self.blueView;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - My Methods

-(void) transformView {
    
    CGFloat rotation;
    
    CGFloat scale = 1.f;
    
    if (self.rotationSwitch.isOn) {
        
        rotation = (float)self.firstSlider.value * M_PI;
    }
    
    if (self.scaleSwitch.isOn) {
        
        scale = self.firstSlider.value + 0.2;
    }
    
    [UIView animateWithDuration:0.7f
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         CGAffineTransform rotationView = CGAffineTransformMakeRotation(rotation);
                         
                         CGAffineTransform scaleView = CGAffineTransformMakeScale(scale, scale);
                         
                         CGAffineTransform transformView = CGAffineTransformConcat(rotationView, scaleView);
                         
                         self.currentView.transform = transformView;
                         
                     }
                     completion:^(BOOL finished) {
                     }
     ];
}

#pragma mark - Actions

- (IBAction)actionTransformEnable:(UISwitch *)sender {

    
}

- (IBAction)actionValueChenge:(UISlider *)sender {
    
    [self transformView];
    
    
}
                   
                   
- (IBAction)actionSelectOfView:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == VGNumberOfViewOne) {
        
        self.currentView = self.blueView;
        
    } else if (sender.selectedSegmentIndex == VGNumberOfViewTwo) {
        
        self.currentView = self.brownView;
        
    } else if (sender.selectedSegmentIndex == VGNumberOfViewThree) {

        self.currentView = self.purpleView;
    }
}

@end
