//
//  ViewController.h
//  ButtonHomeWork
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *entryFieldLabel;

- (IBAction)actionDataInput:(UIButton *)sender;
- (IBAction)actionMethodChoice:(UIButton *)sender;

@end

