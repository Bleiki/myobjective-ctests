//
//  ViewController.m
//  ButtonHomeWork
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(strong, nonatomic) NSMutableString* textInField;
@property(assign, nonatomic) CGFloat firstNumber;
@property(assign, nonatomic) CGFloat secondNumber;
@property(assign, nonatomic) NSInteger currentOperation;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textInField = [[NSMutableString alloc] initWithString:@""];
    self.firstNumber = 0.f;
    self.secondNumber = 0.f;
    self.currentOperation = 0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)actionDataInput:(UIButton *)sender {

    NSRange dot = [self.textInField rangeOfString:@"."];
    
    switch (sender.tag) {
            
        case 10:
            
            if (dot.location == NSNotFound) {

            [self.textInField appendString:@"."];
                
            }
            
            break;
            
        case 314:
            
            [self.textInField appendString:@"3.14"];
            
            break;
            
        default:
            
            [self.textInField appendString:[NSString stringWithFormat:@"%li", sender.tag]];
            
            break;
    }
    
    self.entryFieldLabel.text = self.textInField;

}


- (IBAction)actionMethodChoice:(UIButton *)sender {
    
    switch (sender.tag) {
            
        //Clear
        case 100:
            
            [self.textInField setString:@""];
            
            self.secondNumber = 0.f;
            
            break;
            
        //All clear
        case 1000:
            
            [self.textInField setString:@""];
            
            self.firstNumber = 0.f;
            
            self.secondNumber = 0.f;
            
            break;
            
        //=
        case 11:
            
            self.secondNumber = (float)[self.textInField floatValue];
            
            CGFloat result = 0.f;
            
            if (self.currentOperation == 12) {
                result = self.firstNumber + self.secondNumber;
                
            } else if (self.currentOperation == 13) {
                result = self.firstNumber - self.secondNumber;

            } else if (self.currentOperation == 14) {
                result = self.firstNumber * self.secondNumber;

            } else if (self.currentOperation == 15) {
                if (self.secondNumber != 0) {
                    
                    result = self.firstNumber / self.secondNumber;
                    
                } else {
                    
                    self.firstNumber = 0.f;
                    
                    self.secondNumber = 0.f;
                }
            }
            
            [self.textInField setString:[NSString stringWithFormat:@"%@", [NSNumber numberWithFloat:result]]];
            
            self.firstNumber = [self.textInField floatValue];
            
            break;
        
        //+
        case 12:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            [self.textInField setString:@""];
            
            self.currentOperation = sender.tag;
            
            break;
         
        //-
        case 13:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            [self.textInField setString:@""];
            
            self.currentOperation = sender.tag;
            
            break;
           
        //*
        case 14:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            [self.textInField setString:@""];
            
            self.currentOperation = sender.tag;
            
            break;
        
        //"/"
        case 15:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            [self.textInField setString:@""];
            
            self.currentOperation = sender.tag;
            
            break;
            
            //"1/x"
        case 16:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            self.firstNumber = 1 / self.firstNumber;
            
            [self.textInField setString:[NSString stringWithFormat:@"%@", [NSNumber numberWithFloat:self.firstNumber]]];
            
            break;
            
            //"x^2"
        case 17:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            self.firstNumber = self.firstNumber * self.firstNumber;
            
            [self.textInField setString:[NSString stringWithFormat:@"%@", [NSNumber numberWithFloat:self.firstNumber]]];
            
            break;
            
            //"x^0.5"
        case 18:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            self.firstNumber = sqrtf(self.firstNumber);
            
            [self.textInField setString:[NSString stringWithFormat:@"%@", [NSNumber numberWithFloat:self.firstNumber]]];
            
            break;
            
            //lg
        case 19:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            self.firstNumber = log10f(self.firstNumber);
            
            [self.textInField setString:[NSString stringWithFormat:@"%@", [NSNumber numberWithFloat:self.firstNumber]]];
            
            break;
            
            //log2
        case 20:
            
            self.firstNumber = (float)[self.textInField floatValue];
            
            self.firstNumber = log2f(self.firstNumber);
            
            [self.textInField setString:[NSString stringWithFormat:@"%@", [NSNumber numberWithFloat:self.firstNumber]]];
            
            break;
        default:
            break;
    }
    
    self.entryFieldLabel.text = self.textInField;
}


@end
