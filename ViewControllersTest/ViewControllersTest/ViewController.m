//
//  ViewController.m
//  ViewControllersTest
//
//  Created by Admin on 04.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Loading

//Вызывается, когда происходит загрузка(инициализация) вьюхи. Можно сказать метод init(их несколько).
-(void) loadView {
    
    [super loadView];
    
    NSLog(@"loadView");
}


//Вызывается, когда вьюха загружена(инициализирована уже). Обычно здесь делается вся инициализация для контроллера.
- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSLog(@"viewDidLoad");
    
    self.view.backgroundColor = [UIColor redColor];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        NSLog(@"iPad");
        
    } else {
        
        NSLog(@"iPhone");
    }
}

#pragma mark - Views

//Вызывается когда вьюха почти видна (почти появилась на экране). viewWillDisappear - вызывается когда вьюха почти удалена(пропадет с экрана). animated - говорит о наличии анимации.
- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    NSLog(@"viewWillAppear");

}


//Вызывается когда вьюха уже видна (появилась на экране). viewDidDisappear - вызывается когда вьюха удалена(пропадет с экрана). animated говорит о наличии анимации при появлении.
- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];

    NSLog(@"viewDidAppear");
}


//Вызывается когда перерисовывается элементы вьюхи(сабвьюхи).
-(void) viewWillLayoutSubviews {
    
    [super viewWillLayoutSubviews];
    
    NSLog(@"viewWillLayoutSubviews");
}


//Вызывается когда элементы вьюхи(сабвьюхи) перерисованы.
-(void) viewDidLayoutSubviews {
    
    [super viewDidLayoutSubviews];
    
    NSLog(@"viewDidLayoutSubviews");
}

#pragma mark - Orientation

//Будет ли переворот у нашего контроллера(анимация переворота)
-(BOOL) shouldAutorotate { //Будет ли переворот у нашего контроллера(анимация переворота)
    return NO;
}


//Ориентации которые поддерживает контроллер(если в настройках девайса не выбрана оринтация которая есть здесь, то ориентация контроллера поддерживаться не будет)
-(UIInterfaceOrientationMask) supportedInterfaceOrientations { //Ориентации которые поддерживает контроллер
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskPortrait;
}


//Вызывается когда девай меняет ориентацию. Ориентация экрана будет изменена на:
-(void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
    NSLog(@"willRotateToInterfaceOrientation from %li to %li", self.interfaceOrientation,toInterfaceOrientation);
}


//Вызывается когда девай меняет ориентацию. Ориентация экрана будет изменена с:
-(void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    NSLog(@"didRotateFromInterfaceOrientation %li to %li", fromInterfaceOrientation, self.interfaceOrientation);
}



#pragma mark - Memory

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
