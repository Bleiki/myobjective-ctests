//
//  ViewController.h
//  OutletsHomeWork
//
//  Created by Admin on 06.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property(strong, nonatomic) IBOutletCollection(UIView) NSArray* miniWhiteSquares;
@property(strong, nonatomic) IBOutletCollection(UIView) NSArray* miniRedSquares;
@property(strong, nonatomic) IBOutletCollection(UIView) NSArray* bigSquares;
@property (strong, nonatomic) IBOutlet UIView *board;

@end

