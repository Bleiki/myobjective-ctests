//
//  VGStudent.m
//  BitwisetTest
//
//  Created by Admin on 02.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "VGStudent.h"

@implementation VGStudent

-(NSString*) answerType:(VGStudentSubjectType) type {
    return self.subjectType & type ? @"YES" : @"NO";
}

-(NSString*) description {
    
    return [NSString stringWithFormat:@"Student studies:\n"
                                    "biology = %@\n"
                                    "math = %@\n"
                                    "development = %@\n"
                                    "engineering = %@\n"
                                    "art = %@\n"
                                    "phycology = %@\n"
                                    "anbatomy = %@\n",
                                    [self answerType:VGStudentSubjectTypeBiologi],
                                    [self answerType:VGStudentSubjectTypeMath],
                                    [self answerType:VGStudentSubjectTypeDevelopment],
                                    [self answerType:VGStudentSubjectTypeEngineering],
                                    [self answerType:VGStudentSubjectTypeArt],
                                    [self answerType:VGStudentSubjectTypePhycology],
                                    [self answerType:VGStudentSubjectTypeAnatomy]];
    
    //Можно и так, но так менее красиво(((
    /*
    self.subjectType & VGStudentSubjectTypeBiologi ? @"YES" : @"NO",
    self.subjectType & VGStudentSubjectTypeMath ? @"YES" : @"NO",
    self.subjectType & VGStudentSubjectTypeDevelopment ? @"YES" : @"NO",
    self.subjectType & VGStudentSubjectTypeEngineering ? @"YES" : @"NO",
    self.subjectType & VGStudentSubjectTypeArt ? @"YES" : @"NO",
    self.subjectType & VGStudentSubjectTypePhycology ? @"YES" : @"NO",
    self.subjectType & VGStudentSubjectTypeAnatomy ? @"YES" : @"NO"
    */
}



@end
