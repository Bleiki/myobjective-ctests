//
//  VGStudent.h
//  BitwisetTest
//
//  Created by Admin on 02.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    VGStudentSubjectTypeBiologi         = 1 << 0,
    VGStudentSubjectTypeMath            = 1 << 1,
    VGStudentSubjectTypeDevelopment     = 1 << 2,
    VGStudentSubjectTypeEngineering     = 1 << 3,
    VGStudentSubjectTypeArt             = 1 << 4,
    VGStudentSubjectTypePhycology       = 1 << 5,
    VGStudentSubjectTypeAnatomy         = 1 << 6

}VGStudentSubjectType;

@interface VGStudent : NSObject

@property(assign, nonatomic) VGStudentSubjectType subjectType;

/*
@property(assign, nonatomic) BOOL studiesBiology;
@property(assign, nonatomic) BOOL studiesMath;
@property(assign, nonatomic) BOOL studiesDevelopment;
@property(assign, nonatomic) BOOL studiesEngineering;
@property(assign, nonatomic) BOOL studiesArt;
@property(assign, nonatomic) BOOL studiesPhycology;
@property(assign, nonatomic) BOOL studiesAnatomy;
*/

@end
