//
//  ASSettingsViewController.m
//  SettingsTest
//
//  Created by Oleksii Skutarenko on 21.12.13.
//  Copyright (c) 2013 Alex Skutarenko. All rights reserved.
//

#import "ASSettingsViewController.h"

@interface ASSettingsViewController ()

@end

static NSString* kSettingsLogin           = @"login";
static NSString* kSettingsPassword        = @"password";
static NSString* kSettingsLevel           = @"level";
static NSString* kSettingsShadows         = @"shadows";
static NSString* kSettingsDetalization    = @"detalization";
static NSString* kSettingsSound           = @"sound";
static NSString* kSettingsMusic           = @"music";

@implementation ASSettingsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self loadSettings];
    
    //Устранение проблемы, которая связана с закрытием таблицы, появившейся клавиатурой
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(notificationKeyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(notificationKeyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Notifications

//Позволяет получить информацию о параметрах клавиатуры(маска ее появления, времяанимации, ее размеры и т.д.). На основе этого мы должны сдвигать нашу таблицу(но чтобы параметры ее сдвига не шли в разрез параметрам анимации клавиатуры). Можно менять отступы или фрейм как вариант.

- (void) notificationKeyboardWillShow:(NSNotification*) notification {
    
    NSLog(@"notificationKeyboardWillShow:\n%@", notification.userInfo);
    
}

- (void) notificationKeyboardWillHide:(NSNotification*) notification {
    
    NSLog(@"notificationKeyboardWillHide:\n%@", notification.userInfo);
    
}

#pragma mark - Save and Load

- (void) saveSettings {
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];//Для сохранения небольших данных(по типу Дикшинари)
    
    [userDefaults setObject:self.loginField.text forKey:kSettingsLogin];
    [userDefaults setObject:self.passwordField.text forKey:kSettingsPassword];
   
    [userDefaults setInteger:self.levelControl.selectedSegmentIndex forKey:kSettingsLevel];
    [userDefaults setBool:self.shadowsSwitch.isOn forKey:kSettingsShadows];
    [userDefaults setInteger:self.detalizationControl.selectedSegmentIndex forKey:kSettingsDetalization];
    [userDefaults setDouble:self.soundSlider.value forKey:kSettingsSound];
    [userDefaults setDouble:self.musicSlider.value forKey:kSettingsMusic];
    
    [userDefaults synchronize];//Сохранение произойдет и в случае падения приложения(тогда когда мы хотим) (занимает время)
}

- (void) loadSettings {
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.loginField.text = [userDefaults objectForKey:kSettingsLogin];
    self.passwordField.text = [userDefaults objectForKey:kSettingsPassword];

    self.levelControl.selectedSegmentIndex = [userDefaults integerForKey:kSettingsLevel];
    self.shadowsSwitch.on = [userDefaults boolForKey:kSettingsShadows];
    self.detalizationControl.selectedSegmentIndex = [userDefaults integerForKey:kSettingsDetalization];
    
    self.soundSlider.value = [userDefaults doubleForKey:kSettingsSound];
    self.musicSlider.value = [userDefaults doubleForKey:kSettingsMusic];
    
}

#pragma mark - Actions

- (IBAction)actionTextChanged:(UITextField *)sender {
    [self saveSettings];
}

- (IBAction)actionValueChanged:(id)sender {
    [self saveSettings];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:self.loginField]) {
        [self.passwordField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
    }
    return NO;
}


@end
