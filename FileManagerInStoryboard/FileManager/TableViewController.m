//
//  TableViewController.m
//  FileManager
//
//  Created by Admin on 04.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@property (strong, nonatomic) NSString* path;
@property (strong, nonatomic) NSArray* contents;
@property (strong, nonatomic) NSString* selectedPath;

@end

@implementation TableViewController

#pragma mark - LoadMethods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.path) {
        
        self.path = @"/Users/admin/Desktop/ForTests";
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions
//?????????????????????????????????????
-(IBAction)actionAdd:(UIBarButtonItem*) sender {
    
    NSString* newFolderName = @"New folder";
    
    NSString* newFolderPath = [self.path stringByAppendingPathComponent:newFolderName];
    
    [[NSFileManager defaultManager] createDirectoryAtPath:newFolderPath withIntermediateDirectories:NO attributes:nil error:nil];
    
    NSMutableArray* files = [NSMutableArray array];
    
    NSMutableArray* folders = [NSMutableArray array];
    
    [folders addObject:newFolderName];
    
    for (NSString* content in self.contents) {
        
        NSInteger index = [self.contents indexOfObject:content];
        
        if ([self isDirectory:index]) {
            [folders addObject:content];
        } else {
            [files addObject:content];
        }
    }

    NSMutableArray* resultArray = [NSMutableArray arrayWithArray:folders];
    
    [resultArray addObjectsFromArray:files];
    
    self.contents = resultArray;
    
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.tableView beginUpdates];
    
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
    [self.tableView endUpdates];
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        }
    });
}

-(IBAction)actionEdit:(UIBarButtonItem*) sender {
    
    BOOL isEditing =  self.tableView.editing;
    
    [self.tableView setEditing:!isEditing animated:YES];
    
    UIBarButtonSystemItem item = UIBarButtonSystemItemEdit;
    
    if (self.tableView.editing) {
        item = UIBarButtonSystemItemDone;
    }
    
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:item
                                                                                target:self
                                                                                action:@selector(actionEdit:)];
    
    [self.navigationItem setRightBarButtonItem:editButton animated:YES];
}


#pragma mark - MyMethods

-(void) setPath:(NSString *)path {
    
    _path = path;
    
    NSError* error = nil;
    
    self.contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path error:&error];
    
    NSMutableArray* files = [NSMutableArray array];
    
    NSMutableArray* folders = [NSMutableArray array];
    
    for (NSString* content in self.contents) {
        
        NSInteger index = [self.contents indexOfObject:content];
        
        if ([self isDirectory:index]) {
            [folders addObject:content];
        } else {
            [files addObject:content];
        }
    }
    
    NSArray* sortedFolders = [folders sortedArrayUsingComparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
        return [obj1 compare:obj2];
    }];
    
    NSArray* sortedFiles = [files sortedArrayUsingComparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
        return [obj1 compare:obj2];
    }];
    
    NSMutableArray* resultArray = [NSMutableArray arrayWithArray:sortedFolders];
    
    [resultArray addObjectsFromArray:sortedFiles];
    
    self.contents = resultArray;
    
    [self.tableView reloadData];
    
    self.navigationItem.title = [self.path lastPathComponent];
}

-(BOOL) isDirectory:(NSInteger) index {
    
    NSString* fileName = [self.contents objectAtIndex:index];
    
    NSString* filePath = [self.path stringByAppendingPathComponent:fileName];
    
    BOOL isDirectory = NO;
    
    [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
    
    return isDirectory;
}

- (NSString*) fileSizeFromValue:(unsigned long long) size {
    
    static NSString* units[] = {@"B", @"KB", @"MB", @"GB", @"TB"};
    static int unitsCount = 5;
    
    int index = 0;
    
    double fileSize = (double)size;
    
    while (fileSize > 1024 && index < unitsCount) {
        fileSize /= 1024;
        index++;
    }
    
    return [NSString stringWithFormat:@"%.2f %@", fileSize, units[index]];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* folderIdentifire = @"FolderIdentifire";
    static NSString* fileIdentifire = @"FileIdentifire";
    
    NSString* fileName = [self.contents objectAtIndex:indexPath.row];
    
    NSString* filePath = [self.path stringByAppendingPathComponent:fileName];
    
    NSDictionary* atributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
    
    if ([self isDirectory:indexPath.row]) {
        
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:folderIdentifire];
        
        cell.textLabel.text = fileName;
        
        cell.detailTextLabel.text = nil;
        
        return cell;
    
    } else {
        
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:fileIdentifire];
        
        cell.textLabel.text = fileName;
        
        cell.detailTextLabel.text = [self fileSizeFromValue:[atributes fileSize]];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableArray* array = [NSMutableArray arrayWithArray:self.contents];
        
        [array removeObjectAtIndex:indexPath.row];
        
        self.contents = array;
        
        [tableView beginUpdates];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        
        [tableView endUpdates];
    }
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self isDirectory:indexPath.row]) {
        
        NSString* fileName = [self.contents objectAtIndex:indexPath.row];
        
        NSString* filePath = [self.path stringByAppendingPathComponent:fileName];
        
        TableViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"TableViewController"];
        
        vc.path = filePath;
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

@end
