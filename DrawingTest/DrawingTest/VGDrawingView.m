//
//  VGDrawingView.m
//  DrawingTest
//
//  Created by Admin on 12.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "VGDrawingView.h"

@implementation VGDrawingView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    //[super drawRect:rect];//Чтобы дополнить
    
    NSLog(@"drawRect %@", NSStringFromCGRect(rect));
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //CGContextSetFillColorWithColor(context, [UIColor redColor].CGColor); // Заполняет цветом.
    
    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor); //Цвет линии.
    
    //CGContextFillRect(context, rect);//Собственно закрашивает или CGContextAddRect(context, rect); CGContextFillPath(context);
    
    //Нарисовал квадраты
    CGRect square1 = CGRectMake(50, 50, 50, 50);
    CGRect square2 = CGRectMake(100, 100, 50, 50);
    CGRect square3 = CGRectMake(150, 150, 50, 50);
    
    CGContextAddRect(context, square1);
    CGContextAddRect(context, square2);
    CGContextAddRect(context, square3);

    //CGContextFillPath(context);
    CGContextStrokePath(context);
    
    //Нарисовл круги
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    
    CGContextAddEllipseInRect(context, square1);
    CGContextAddEllipseInRect(context, square2);
    CGContextAddEllipseInRect(context, square3);
    
    CGContextFillPath(context);
    
    //Нарисовл линию
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
    CGContextSetLineWidth(context, 3);//Ширина линии
    CGContextSetLineCap(context, kCGLineCapRound);//Конец линии будет закругленныйю
    
    CGContextMoveToPoint(context, CGRectGetMinX(square1), CGRectGetMaxY(square1));
    CGContextAddLineToPoint(context, CGRectGetMinX(square3), CGRectGetMaxY(square3));
    
    CGContextStrokePath(context);
    
    //Нарисовал 2 линию
    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
    
    CGContextSetLineWidth(context, 3);//Ширина линии
    CGContextSetLineCap(context, kCGLineCapRound);//Конец линии будет закругленныйю
    
    CGContextMoveToPoint(context, CGRectGetMaxX(square1), CGRectGetMinY(square1));
    CGContextAddLineToPoint(context, CGRectGetMaxX(square3), CGRectGetMinY(square3));
    
    CGContextStrokePath(context);

    //Нарисовал часть круга
    CGContextMoveToPoint(context, CGRectGetMinX(square1), CGRectGetMaxY(square1));
    CGContextAddArc(context, CGRectGetMaxX(square1), CGRectGetMaxY(square1), CGRectGetWidth(square1), M_PI,  M_PI_2, YES);
    //Арка: контекст, координаты центра х и у, радиус, начальный и конечный углы(не как в школьной геометрии (направление углов в другую сторону)) и направление(по часовой стрелке(да или нет)).
    
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    
    CGContextStrokePath(context);
    
    //Нарисовал текст
    NSString* text = @"Test";
    
    UIFont* font = [UIFont systemFontOfSize:14.f];
    
    NSShadow* shadow = [[NSShadow alloc] init];
    shadow.shadowOffset = CGSizeMake(1, 1);
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowBlurRadius = 0.5f;
    
    NSDictionary* atributes = [NSDictionary dictionaryWithObjectsAndKeys:
                               [UIColor grayColor],     NSForegroundColorAttributeName,
                               font,                    NSFontAttributeName,
                               shadow,                  NSShadowAttributeName,
                               nil];
    
    CGSize textSize = [text sizeWithAttributes:atributes];//Определяет размеры текста и вводит поправку, чтобы центр текста был в центре, а не его левый верхний угол. Не очень надежный так как сдвиг м.б. на пол пикселя и из-за этого изображение бует размытым.
    
    //[text drawAtPoint:CGPointMake(CGRectGetMidX(square2) - textSize.width / 2, CGRectGetMidY(square2) - textSize.height / 2) withAttributes:atributes];
    
    CGRect textRect = CGRectMake(CGRectGetMidX(square2) - textSize.width / 2,
                                 CGRectGetMidY(square2) - textSize.height / 2,
                                 textSize.width,
                                 textSize.height);
    
    textRect = CGRectIntegral(textRect);//Округляет неточность, и тем самым устраняет размытость изображения(округляет).
    
    [text drawInRect:textRect withAttributes:atributes];
    
    
    
    
    
    //Шахматная доска
    /*
    NSLog(@"drawRect %@", NSStringFromCGRect(rect));
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat offset = 70.f;//Отступ
    CGFloat borderWidth = 4.f;//Рамка
    
    CGFloat maxBoardSize = MIN(CGRectGetWidth(rect) - offset * 2 - borderWidth * 2,
                               CGRectGetHeight(rect) - offset * 2 - borderWidth * 2);

    int cellSize = (int)maxBoardSize / 8;//Избавились от дробности
    int boardSize = cellSize * 8;//Избавились от дробности
    
    //Рисуем клетки
    CGRect boardRect = CGRectMake((CGRectGetWidth(rect) - boardSize) / 2,
                                  (CGRectGetHeight(rect) - boardSize) / 2,
                                  boardSize, boardSize);
    
    boardRect = CGRectIntegral(boardRect);
    
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            if (i % 2 != j % 2) {
                CGRect cellRect = CGRectMake(CGRectGetMinX(boardRect) + i * cellSize,
                                             CGRectGetMinY(boardRect) + j * cellSize,
                                             cellSize, cellSize);
                CGContextAddRect(context, cellRect);
            }
        }
    }
    
    CGContextSetFillColorWithColor(context, [UIColor grayColor].CGColor);
    
    CGContextFillPath(context);
    
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);

    CGContextAddRect(context, boardRect);
    
    CGContextSetLineWidth(context, borderWidth);
    
    CGContextStrokePath(context);
    */
    
}


@end
