//
//  ASViewController.m
//  TextFieldsTest
//
//  Created by Oleksii Skutarenko on 13.12.13.
//  Copyright (c) 2013 Alex Skutarenko. All rights reserved.
//

#import "ASViewController.h"

@interface ASViewController () 

@end

@implementation ASViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
    //self.firstNameField.keyboardAppearance = UIKeyboardAppearanceDark;
    //self.lastNameField.keyboardAppearance = UIKeyboardAppearanceLight;
    
    [self.firstNameField becomeFirstResponder];//Фокус становится в поле при загрузке
    
    NSNotificationCenter* nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(notificationTextDidBeginEditing:) name:UITextFieldTextDidBeginEditingNotification object:nil];//Подписываемся на нотификацию
    [nc addObserver:self selector:@selector(notificationTextDidEndEditing:) name:UITextFieldTextDidEndEditingNotification object:nil];
    [nc addObserver:self selector:@selector(notificationTextDidChangeEditing:) name:UITextFieldTextDidChangeNotification object:nil];
    
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];//Отписываемся от нотификации
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

//Нажатие кнопки ввода
- (IBAction) actionLog:(id)sender {
    
    NSLog(@"First Name = %@, Last Name = %@", self.firstNameField.text, self.lastNameField.text);
    
    if ([self.firstNameField isFirstResponder]) {//Проверяем является ли активным(в фокусе ли он)
        [self.firstNameField resignFirstResponder];
    } else if ([self.lastNameField isFirstResponder]) {
        [self.lastNameField resignFirstResponder];
    }
}

//Сохраняются все буквы
- (IBAction)actionTextChanged:(UITextField *)sender {
    
    NSLog(@"%@", sender.text);
    
}

#pragma mark - UITextFieldDelegate

//Каждому полю надо установить делегата - главное вью (в сторибоарде)
/*
 //Редактирование поля
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    return [textField isEqual:self.firstNameField];
    
}

 //Очистка поля
- (BOOL)textFieldShouldClear:(UITextField *)textField {
    return NO;
}
 */

//Нажатие кнопки "далее"
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:self.firstNameField]) {
        [self.lastNameField becomeFirstResponder];// Объект который становится активным
    } else {
        [textField resignFirstResponder];//Объект перестает быть активным (уезжает клавиатура)
    }
    
    return YES;
}

#pragma mark - Notifications

- (void) notificationTextDidBeginEditing:(NSNotification*) notification {
    NSLog(@"notificationTextDidBeginEditing");
}

- (void) notificationTextDidEndEditing:(NSNotification*) notification {
    NSLog(@"notificationTextDidEndEditing");
}

- (void) notificationTextDidChangeEditing:(NSNotification*) notification {
    NSLog(@"notificationTextDidChangeEditing");
}

@end
