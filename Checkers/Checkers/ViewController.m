//
//  ViewController.m
//  Checkers
//
//  Created by Admin on 09.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(strong, nonatomic) UIView* board;
@property(weak, nonatomic) UIView* draggingView;
@property(assign, nonatomic) CGPoint touchOffset;
@property(assign, nonatomic) CGPoint startingPoint;
@property(strong, nonatomic) NSMutableArray* arrayOfBlackSquares;
@property(strong, nonatomic) NSMutableArray* arrayOfCheckers;
@property(strong, nonatomic) NSMutableDictionary* dictionaryOfRanges;

@end

@implementation ViewController


#pragma mark - RedefinedFunctions

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayOfBlackSquares = [NSMutableArray array];
    self.arrayOfCheckers = [NSMutableArray array];
    self.dictionaryOfRanges = [NSMutableDictionary dictionary];
    
    //Created playing field.
    self.view.backgroundColor = [UIColor grayColor];
    self.board = [self board:self.view withColor:[UIColor whiteColor]];
    [self.view addSubview:self.board];
    
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 4; j++) {
            
            UIView* blackSquare = [self blackSquare:self.board withLine:i andColumn:j withColor:[UIColor blackColor]];
            
            [self.board addSubview:blackSquare];
            
            [self.arrayOfBlackSquares addObject:blackSquare];
        }
    }
    
    //Created red checkers.
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            
            UIColor* color = [UIColor redColor];
            
            UIView* checker = [self checker:self.board withLine:i andColumn:j withColor:color];
            
            [self.board addSubview:checker];
            
            [self.arrayOfCheckers addObject:checker];
        }
    }
    
    //Created white checkers.
    for (int i = 5; i < 8; i++) {
        for (int j = 0; j < 4; j++) {
            
            UIColor* color = [UIColor greenColor];
            
            UIView* checker = [self checker:self.board withLine:i andColumn:j withColor:color];
            
            [self.board addSubview:checker];
            
            [self.arrayOfCheckers addObject:checker];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Tuches

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self logTouches:touches withMethod:@"touchesBegan"];
    
    UITouch* touch = [touches anyObject];
    
    CGPoint pointOnBoard = [touch locationInView:self.board];
    
    UIView* view = [self.board hitTest:pointOnBoard withEvent:event];
    
    if ((![view isEqual:self.board]) && (![self.arrayOfBlackSquares containsObject:view])) {
        
        self.draggingView = view;

        self.startingPoint = CGPointMake(CGRectGetMidX(view.frame), CGRectGetMidY(view.frame)); //Memorization of starting place

        [self.board bringSubviewToFront:self.draggingView];
        
        //Selection correction new point.
        CGPoint pointOfTouch = [touch locationInView:self.draggingView];
        
        self.touchOffset = CGPointMake(CGRectGetMidX(self.draggingView.bounds) - pointOfTouch.x,
                                       CGRectGetMidY(self.draggingView.bounds) - pointOfTouch.y);
        
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.draggingView.transform = CGAffineTransformMakeScale(1.2f, 1.2f);
                             self.draggingView.alpha = 0.5f;
                         }];
    } else {
        self.draggingView = nil;
    }
}


-(void) touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self logTouches:touches withMethod:@"touchesMoved"];
    
    if (self.draggingView) {
        
        UITouch* touch = [touches anyObject];
        
        CGPoint pointOnBoard = [touch locationInView:self.board];
        
        CGPoint correction = CGPointMake(pointOnBoard.x + self.touchOffset.x,
                                         pointOnBoard.y + self.touchOffset.y);
        
        self.draggingView.center = correction;
    }
}


-(void) touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self logTouches:touches withMethod:@"touchesEnded"];
    
    [self endOfTouch:touches withEvent:event];
}


-(void) touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self logTouches:touches withMethod:@"touchesCancelled"];

    [self cancelOfTouch:touches withEvent:event];
}


#pragma mark - MyFunctions


-(void) endOfTouch:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //Checker go to the center of square.
    [self checkerGoToTheCenterOfSquare:touches withEvent:event];
    
    //If the checker get on white square, then she's gets to the starting point.
    //[self ifTouchOnWhiteSquareThenGoToStartingPoint];
    
    //If the checker get on white square, then she's gets to the nearest black square.
    [self ifTouchOnWhiteSquareThenGoToNearstSquare:touches withEvent:event];
    
    //If the checker comes out of board, then she's gets back to starting point.
    [self ifTouchDidNotGetOnBoardThenGoToStartingPoint:touches withEvent:event];
    
    //If the checker get on checker, then she's gets back to it's place.
    [self ifCheckerGetOnOtherCheckerThenGoToStartingPoint];
    
    //End of animation.
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.draggingView.transform = CGAffineTransformIdentity;
                         self.draggingView.alpha = 1.f;
                     }];
 
    self.draggingView = nil;
}





-(void) cancelOfTouch:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.draggingView.transform = CGAffineTransformIdentity;
                         self.draggingView.alpha = 1.f;
                     }];
    
    self.draggingView.center = self.startingPoint;
    
    self.draggingView = nil;
}


-(void) logTouches:(NSSet*) touches withMethod:(NSString*) methodName {
    
    NSMutableString* string = [NSMutableString stringWithString:methodName];
    
    for (UITouch* touch in touches) {
        
        CGPoint pointOfTouch = [touch locationInView:self.board];
        
        [string appendFormat:@"%@", NSStringFromCGPoint(pointOfTouch)];
    }
    
    NSLog(@"%@", string);
}


-(UIView*) checker:(UIView*) view withLine:(NSInteger) i andColumn:(NSInteger) j withColor:(UIColor*) color {
    
    CGRect square = CGRectMake(CGRectGetWidth(view.bounds) / 32 + (CGRectGetWidth(view.bounds) / 4) * j,
                               CGRectGetHeight(view.bounds) / 32 + (CGRectGetHeight(view.bounds) / 8) * i,
                               CGRectGetWidth(view.bounds) / 16,
                               CGRectGetWidth(view.bounds) / 16);
    
    if (!(i % 2)) {
        
        square.origin.x += CGRectGetWidth(view.bounds) / 8;
        
    }
    
    UIView* checker = [[UIView alloc] initWithFrame:square];
    
    checker.backgroundColor = color;
    
    return checker;
}


-(UIView*) blackSquare:(UIView*) view withLine:(NSInteger) i andColumn:(NSInteger) j withColor:(UIColor*) color {
    
    CGRect square = CGRectMake(0 + (CGRectGetWidth(view.bounds) / 4) * j,
                               0 + (CGRectGetHeight(view.bounds) / 8) * i,
                               CGRectGetWidth(view.bounds) / 8,
                               CGRectGetWidth(view.bounds) / 8);
    
    if (!(i % 2)) {
        square.origin.x += CGRectGetWidth(view.bounds) / 8;
    }
    
    UIView* blackSquare = [[UIView alloc] initWithFrame:square];
    
    blackSquare.backgroundColor = color;
    
    return blackSquare;
}


-(UIView*) board:(UIView*) view withColor:(UIColor*) color {
    
    CGRect myBoard = CGRectMake(CGRectGetMinX(view.bounds),
                                (CGRectGetHeight(view.bounds) - CGRectGetWidth(view.bounds)) / 2,
                                CGRectGetWidth(view.bounds),
                                CGRectGetWidth(view.bounds));
    
    UIView* board = [[UIView alloc] initWithFrame:myBoard];
    
    board.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    board.backgroundColor = color;
    
    return board;
}


#pragma mark - MethodsForEndOfTouch

-(void) checkerGoToTheCenterOfSquare:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {

    UITouch* touch = [touches anyObject];
    
    CGPoint pointOnBoard = [touch locationInView:self.board];
    
    if (self.draggingView) {
        
        for (UIView* bigView in self.arrayOfBlackSquares) {
            
            BOOL containsToSquare = CGRectContainsPoint(bigView.frame, pointOnBoard);
            
            if (containsToSquare) {
                
                CGPoint newCenter = CGPointMake(CGRectGetMidX(bigView.frame), CGRectGetMidY(bigView.frame));
                
                self.draggingView.center = newCenter;
                
            }
        }
    }
}


-(void) ifTouchOnWhiteSquareThenGoToStartingPoint {
    
    NSInteger count = 0;
    
    for (UIView* bigView in self.arrayOfBlackSquares) {
        
        BOOL containsToSquare = CGRectContainsPoint(bigView.frame, self.draggingView.center);
        
        count = (!containsToSquare) ? (count + 1) : (count + 0);
        
        if (count == [self.arrayOfBlackSquares count]) {
            
            self.draggingView.center = self.startingPoint;
        }
    }
}


-(void) ifTouchOnWhiteSquareThenGoToNearstSquare:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    UITouch* touch = [touches anyObject];
    
    CGPoint pointOnBoard = [touch locationInView:self.board];
    
    NSInteger count = 0;
    
    for (UIView* bigView in self.arrayOfBlackSquares) {
        
        BOOL containsToSquare = CGRectContainsPoint(bigView.frame, self.draggingView.center);
        
        count = (!containsToSquare) ? (count + 1) : (count + 0);
    }
    
    if (count == [self.arrayOfBlackSquares count]) {
        
        for (UIView* bigView in self.arrayOfBlackSquares) {
            
            CGFloat range = sqrt(powf((pointOnBoard.x - CGRectGetMidX(bigView.frame)), 2.f) +
                                 powf((pointOnBoard.y - CGRectGetMidY(bigView.frame)), 2.f));
            
            NSNumber* floatRange = [NSNumber numberWithFloat:range];
            
            [self.dictionaryOfRanges setObject:bigView forKey:floatRange];
        }
        NSArray* sortedKeys = [self.dictionaryOfRanges allKeys];
        
        sortedKeys = [sortedKeys sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            return [obj1 compare:obj2];
        }];
        
        UIView* amendment = [self.dictionaryOfRanges objectForKey:[sortedKeys objectAtIndex:0]];
        
        self.draggingView.center = CGPointMake(CGRectGetMidX(amendment.frame) , CGRectGetMidY(amendment.frame));
        
        [self.dictionaryOfRanges removeAllObjects];
    }
}


-(void) ifTouchDidNotGetOnBoardThenGoToStartingPoint:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    UITouch* touch = [touches anyObject];
    
    CGPoint pointOnBigView = [touch locationInView:self.view];
    
    BOOL containsToBoard = CGRectContainsPoint(self.board.frame, pointOnBigView);
    
    if (!containsToBoard) {
        self.draggingView.center = self.startingPoint;
    }
}


-(void) ifCheckerGetOnOtherCheckerThenGoToStartingPoint {
    
    if (self.draggingView) {
        
        [self.arrayOfCheckers removeObject:self.draggingView];
        
        for (UIView* miniView in self.arrayOfCheckers) {
            
            BOOL containsToMiniSquare = CGRectContainsPoint(miniView.frame, self.draggingView.center);
            
            if (containsToMiniSquare) {
                self.draggingView.center = self.startingPoint;
            }
        }
        
        [self.arrayOfCheckers addObject:self.draggingView];
    }
}

@end
