//
//  AppDelegate.h
//  Checkers
//
//  Created by Admin on 09.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

