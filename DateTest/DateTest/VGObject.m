//
//  VGObject.m
//  DateTest
//
//  Created by Admin on 03.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "VGObject.h"

@implementation VGObject

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSLog(@"VGObject is initiated");
        
        NSTimer* timer = [NSTimer scheduledTimerWithTimeInterval:2.5 target:self selector:@selector(timerTest:) userInfo:nil repeats:YES];
        
        [timer setFireDate:[NSDate dateWithTimeIntervalSinceNow:5]];

    }
    return self;
}


-(void) timerTest:(NSTimer*) timer {
    
    NSDateFormatter* dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:@"HH:mm:ss:SSS"];
    NSLog(@"%@", [dateFormater stringFromDate:[NSDate date]]);
    
    [timer invalidate]; //Уничтожение таймера
}


- (void)dealloc {
    
    NSLog(@"VGObject is deallocated");
}


@end
