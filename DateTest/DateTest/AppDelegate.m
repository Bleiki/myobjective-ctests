//
//  AppDelegate.m
//  DateTest
//
//  Created by Admin on 03.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "AppDelegate.h"
#import "VGObject.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    NSDate* date = [NSDate date];
    NSLog(@"%@", date);
    
    NSLog(@"%@", [date dateByAddingTimeInterval:5000]); //5000 In sec
    NSLog(@"%@", [date dateByAddingTimeInterval:-5000]);

    [date compare:[date dateByAddingTimeInterval:-5000]]; //Сравнение
    [date earlierDate:[date dateByAddingTimeInterval:-5000]]; //Вернет меньшую дату (позднейшую дату: laterDate)
    
    
    NSDate* date2 = [NSDate dateWithTimeIntervalSinceReferenceDate:10];// From 01.01.2001
    NSLog(@"%@", date2);
    
    
    
    //Форматы вывода даты (широкий простор для опытов)
    NSDate* date = [NSDate date];
    
    NSDateFormatter* dateFormater = [[NSDateFormatter alloc] init];
    
    [dateFormater setDateStyle:NSDateFormatterShortStyle];
    NSLog(@"%@", [dateFormater stringFromDate:date]);
    
    [dateFormater setDateStyle:NSDateFormatterMediumStyle];
    NSLog(@"%@", [dateFormater stringFromDate:date]);
    
    [dateFormater setDateStyle:NSDateFormatterLongStyle];
    NSLog(@"%@", [dateFormater stringFromDate:date]);
    
    [dateFormater setDateStyle:NSDateFormatterFullStyle];
    NSLog(@"%@", [dateFormater stringFromDate:date]);
    
    [dateFormater setDateFormat:@"yyyy MM MMM MMMM MMMMM"];
    
    [dateFormater setDateFormat:@"yyyy/MM/dd/DD EEE EEEE EEEEE"]; //dd-день в месяце, ЕЕЕ-день недели, DD-День в году
    
    [dateFormater setDateFormat:@"yyyy/MM/dd EEEE HH:mm/SSS"]; //HH(0-24 hours) hh(0-12 hours) a(PM) W(неделя месяца) w(неделя года) SSS(мили секунды) уу(две последних цифры года) Можно выводить эру
    
    [dateFormater setDateFormat:@"yyyy/MM/dd HH:mm"];
    
    NSLog(@"%@", [dateFormater stringFromDate:date]);

    
    NSDate* date3 = [dateFormater dateFromString:@"2008/05/17 15:37"];
    NSLog(@"%@", date3);
    
    
    NSInteger year = [@"2013" integerValue];
    
    //Получение даты целым числом
    
    NSDate* date = [NSDate date];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components =
    [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay |
        NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
                fromDate:date];
    
    NSLog(@"%@", components);
    
    NSInteger hour = [components hour];
    
    NSLog(@"%li", hour);

    
    //Какая разница между датами
    NSDate* date1 = [NSDate date];
    NSDate* date2 = [NSDate dateWithTimeIntervalSinceNow:-1000000];

    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* components =
    [calendar components:NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute
                fromDate:date2
                  toDate:date1
                 options:0];
    
    NSLog(@"%@", components);
    
    
    //Таймер (in VGObject)    

    VGObject* obj = [[VGObject alloc] init];
    
    
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
