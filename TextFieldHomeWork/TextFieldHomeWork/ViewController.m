//
//  ViewController.m
//  TextFieldHomeWork
//
//  Created by Admin on 27.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

typedef enum {
    
    VGTextFieldNameFirstName,
    VGTextFieldNameLastName,
    VGTextFieldNameLogin,
    VGTextFieldNamePassword,
    VGTextFieldNameAge,
    VGTextFieldNamePhoneNumber,
    VGTextFieldNameEMail
    
}VGTextFieldName;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[self.textFields objectAtIndex:VGTextFieldNameFirstName] becomeFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

-(IBAction) actionTextChange:(UITextField*) sender {
    
    NSInteger i = [self.textFields indexOfObject:sender];
    
    UILabel* currentLabel = [self.lablesCollection objectAtIndex:i];
    
    currentLabel.text = sender.text;
}


#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:[self.textFields objectAtIndex:VGTextFieldNamePhoneNumber]]) {
        
        NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
        
        NSArray* figures = [string componentsSeparatedByCharactersInSet:validationSet];
        
        //Пропускает только цифры
        if ([figures count] > 1) {
            return NO;
        }
        
        NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];//Вставляет строку в рэндж нашей строки
        
        //Пришло: +XX (XXX) XXXX-XXX
        NSArray* figuresOfNewString = [newString componentsSeparatedByCharactersInSet:validationSet];
        
        newString = [figuresOfNewString componentsJoinedByString:@""];
        //Теперь XXXXXXXXXXXX
        
        static const int maxLocalNumberLength = 7;
        static const int maxVillageCodeLength = 3;
        static const int maxCountryCodeLength = 3;
        
        //Ограничение длины номера
        if (newString.length > maxLocalNumberLength + maxVillageCodeLength + maxCountryCodeLength) {
            return NO;
        }
        
        NSMutableString* resultString = [NSMutableString string];
        
        if (newString.length > 0) {
            
            NSInteger localNumberLength = MIN(newString.length, maxLocalNumberLength);
            
            NSString* localNumber = [newString substringFromIndex:(int)newString.length - localNumberLength];
                                     
            [resultString appendString:localNumber];
            
            if (resultString.length > 3) {
                [resultString insertString:@"-" atIndex:3];
            }
            
            if (resultString.length > 6) {
                [resultString insertString:@"-" atIndex:6];
            }
        }
        
        if (newString.length > maxLocalNumberLength) {
            
            NSInteger villageCodeLength = MIN(newString.length - maxLocalNumberLength, maxVillageCodeLength);
            
            NSRange villageCodeRange = NSMakeRange(newString.length - maxLocalNumberLength - villageCodeLength, villageCodeLength);
            
            NSString* villageCode = [newString substringWithRange:villageCodeRange];
            
            villageCode = [NSString stringWithFormat:@"(%@) ", villageCode];
            
            [resultString insertString:villageCode atIndex:0];
        }
        
        if (newString.length > maxLocalNumberLength + maxVillageCodeLength) {
            
            NSInteger countryCodeLength = MIN(newString.length - maxLocalNumberLength - maxVillageCodeLength, maxCountryCodeLength);
            
            NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
            
            NSString* countryCode = [newString substringWithRange:countryCodeRange];
            
            countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
            
            [resultString insertString:countryCode atIndex:0];
        }
        
        textField.text = resultString;
        
        UILabel* currentLabel = [self.lablesCollection objectAtIndex:VGTextFieldNamePhoneNumber];
        
        currentLabel.text = resultString;
        
        return NO;
        
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    } else if ([textField isEqual:[self.textFields objectAtIndex:VGTextFieldNameEMail]]) {
        
        NSMutableCharacterSet* validationSet = [NSMutableCharacterSet alphanumericCharacterSet];
        
        [validationSet addCharactersInString:@"@._-"];
        
        [validationSet invert];
        
        NSArray* characters = [string componentsSeparatedByCharactersInSet:validationSet];
        
        NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        //Пропускает только допустимые символы и ограничение длины E-mail
        if ([characters count] > 1 || newString.length > 24) {
            return NO;
        }
        
        //Ограничение ввода "@"
        NSRange rangeInTextField = [textField.text rangeOfString:@"@"];
        
        NSRange rangeInString = [string rangeOfString:@"@"];
        
        if (rangeInTextField.location != NSNotFound && rangeInString.location != NSNotFound) {
            return NO;
        }
        
        //Вывод в Label
        UILabel* currentLabel = [self.lablesCollection objectAtIndex:VGTextFieldNameEMail];
        
        currentLabel.text = string;
        
        return YES;
        
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    } else if ([textField isEqual:[self.textFields objectAtIndex:VGTextFieldNameAge]]) {
        
        NSCharacterSet* validationSet = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
        
        NSArray* characters = [string componentsSeparatedByCharactersInSet:validationSet];
        
        NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        //Пропускает только допустимые символы и ограничение длины возраста
        if ([characters count] > 1 || newString.intValue > 120) {
            return NO;
        }
        
        //Вывод в Label
        UILabel* currentLabel = [self.lablesCollection objectAtIndex:VGTextFieldNameAge];
        
        currentLabel.text = string;
        
        return YES;
    
    } else {
        return YES;
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    for (NSInteger i = 0; i < [self.textFields count]; i++) {
        
        UITextField* field = [self.textFields objectAtIndex:i];
        
        if ([textField isEqual:field]) {
            if (i == VGTextFieldNameEMail) {
                
                [textField resignFirstResponder];
                
            } else {
                
                UITextField* nextField = [self.textFields objectAtIndex:i + 1];

                [nextField becomeFirstResponder];
            }
        }
    }
    
    return YES;
}


- (BOOL)textFieldShouldClear:(UITextField *)textField {
    
    NSInteger i = [self.textFields indexOfObject:textField];
    
    UILabel* currentLabel = [self.lablesCollection objectAtIndex:i];
    
    currentLabel.text = @"";
    
    return YES;
}

@end
