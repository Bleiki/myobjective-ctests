//
//  ViewController.h
//  TextFieldHomeWork
//
//  Created by Admin on 27.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFields;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *lablesCollection;

-(IBAction) actionTextChange:(UITextField*) sender;

@end

