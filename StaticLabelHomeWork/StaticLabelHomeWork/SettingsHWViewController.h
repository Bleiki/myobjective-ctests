//
//  SettingsHWViewController.h
//  StaticLabelHomeWork
//
//  Created by Admin on 29.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsHWViewController : UITableViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *textFieldsCollection;

@property (weak, nonatomic) IBOutlet UISlider *ageSlider;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *educationControl;
@property (weak, nonatomic) IBOutlet UISwitch *hostelSwitch;

- (IBAction)textChange:(UITextField *)sender;
- (IBAction)actionValueChange:(id)sender;


@end
