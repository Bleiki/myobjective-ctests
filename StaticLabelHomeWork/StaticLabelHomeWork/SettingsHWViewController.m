//
//  SettingsHWViewController.m
//  StaticLabelHomeWork
//
//  Created by Admin on 29.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "SettingsHWViewController.h"

@interface SettingsHWViewController ()

@end

typedef enum {
    
    VGTextFieldNameFirstName,
    VGTextFieldNameLastName,
    VGTextFieldNamePhoneNumber,
    VGTextFieldNameEducationalInstitute,
    VGTextFieldNameTrainingPeriod,
    VGTextFieldNameLogin,
    VGTextFieldNamePassword,
    VGTextFieldNameConfirmPassword,
    VGTextFieldNameEmail
    
}VGTextFieldName;

static NSString* kAgeSlider = @"slider";
static NSString* kAgeLabel = @"label";
static NSString* kEducationControl = @"control";
static NSString* kHostelSwitch = @"switch";


@implementation SettingsHWViewController

#pragma mark - Standart methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIEdgeInsets inset = UIEdgeInsetsMake(20, 0, 0, 0);
    
    self.tableView.contentInset = inset;
    
    self.tableView.scrollIndicatorInsets = inset;
    
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Save and Load Methods

-(void) saveData {
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setDouble:self.ageSlider.value forKey:kAgeSlider];
    [userDefaults setObject:self.ageLabel.text forKey:kAgeLabel];
    [userDefaults setInteger:self.educationControl.selectedSegmentIndex forKey:kEducationControl];
    [userDefaults setBool:self.hostelSwitch.on forKey:kHostelSwitch];
    
    for (UITextField* field in self.textFieldsCollection) {
        
        NSInteger index = [self.textFieldsCollection indexOfObject:field];
        
        NSNumber* indexOfField = [NSNumber numberWithInteger:index];
        
        NSString* keyForTextField = [NSString stringWithFormat:@"%@", indexOfField];
        
        [userDefaults setObject:field.text forKey:keyForTextField];
    }
    
    [userDefaults synchronize];
}

-(void) loadData {
    
    NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.ageSlider.value = [userDefaults doubleForKey:kAgeSlider];
    self.ageLabel.text = [userDefaults objectForKey:kAgeLabel];
    self.educationControl.selectedSegmentIndex = [userDefaults integerForKey:kEducationControl];
    self.hostelSwitch.on = [userDefaults boolForKey:kHostelSwitch];
    
    for (UITextField* field in self.textFieldsCollection) {
        
        NSInteger index = [self.textFieldsCollection indexOfObject:field];
        
        NSNumber* indexOfField = [NSNumber numberWithInteger:index];
        
        NSString* keyForTextField = [NSString stringWithFormat:@"%@", indexOfField];
        
        field.text = [userDefaults objectForKey:keyForTextField];
    }
}


#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNameFirstName]] ||
        [textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNameLastName]]) {
        
        return [self correctName:textField shouldChangeCharactersInRange:range replacementString:string];
        
    }else if ([textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNameEducationalInstitute]]) {
        
        return [self correctInstitute:textField shouldChangeCharactersInRange:range replacementString:string];
        
    } else if ([textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNamePhoneNumber]]) {
        
        return [self correctPhoneNumber:textField shouldChangeCharactersInRange:range replacementString:string];
        
    } else if ([textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNameTrainingPeriod]]) {
        
        return [self correctTrainingPeriod:textField shouldChangeCharactersInRange:range replacementString:string];
        
    } else if ([textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNameLogin]] ||
               [textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNamePassword]] ||
               [textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNameConfirmPassword]]) {
        
        return [self correctLoginAndPassword:textField shouldChangeCharactersInRange:range replacementString:string];
        
    } else if ([textField isEqual:[self.textFieldsCollection objectAtIndex:VGTextFieldNameEmail]]) {
        
        return [self correctEmail:textField shouldChangeCharactersInRange:range replacementString:string];
        
    } else {
        
        return YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    for (NSInteger i = 0; i < [self.textFieldsCollection count]; i++) {
        
        UITextField* field = [self.textFieldsCollection objectAtIndex:i];
        
        if ([textField isEqual:field]) {
            if (i == VGTextFieldNameEmail) {
                
                [textField resignFirstResponder];
                
            } else {
                
                UITextField* nextField = [self.textFieldsCollection objectAtIndex:i + 1];
                
                [nextField becomeFirstResponder];
            }
        }
    }
    return YES;
}


#pragma mark - Methods for Delegate

-(BOOL) correctName:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string{
    
    NSCharacterSet* validationSet = [[NSCharacterSet letterCharacterSet] invertedSet];
    
    NSArray* characters = [string componentsSeparatedByCharactersInSet:validationSet];
    
    NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    //Пропускает только допустимые символы и ограничение длины
    if ([characters count] > 1 || newString.length > 15) {
        return NO;
    }
    
    return YES;
}

-(BOOL) correctInstitute:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSMutableCharacterSet* validationSet = [NSMutableCharacterSet letterCharacterSet];
    
    [validationSet addCharactersInString:@" -"];
    
    [validationSet invert];
    
    NSArray* characters = [string componentsSeparatedByCharactersInSet:validationSet];
    
    NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    //Пропускает только допустимые символы и ограничение длины
    if ([characters count] > 1 || newString.length > 30) {
        return NO;
    }
    
    return YES;
}

-(BOOL) correctPhoneNumber:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString*)string {
    
    NSCharacterSet* validationSet = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];

    NSArray* figures = [string componentsSeparatedByCharactersInSet:validationSet];

    //Пропускает только цифры
    if ([figures count] > 1) {
        return NO;
    }

    NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];//Вставляет строку в рэндж нашей строки

    //Пришло: +XX (XXX) XXXX-XXX
    NSArray* figuresOfNewString = [newString componentsSeparatedByCharactersInSet:validationSet];

    newString = [figuresOfNewString componentsJoinedByString:@""];
    //Теперь XXXXXXXXXXXX

    static const int maxLocalNumberLength = 7;
    static const int maxVillageCodeLength = 3;
    static const int maxCountryCodeLength = 3;

    //Ограничение длины номера
    if (newString.length > maxLocalNumberLength + maxVillageCodeLength + maxCountryCodeLength) {
        return NO;
    }

    NSMutableString* resultString = [NSMutableString string];

    if (newString.length > 0) {
    
        NSInteger localNumberLength = MIN(newString.length, maxLocalNumberLength);
    
        NSString* localNumber = [newString substringFromIndex:(int)newString.length - localNumberLength];
    
        [resultString appendString:localNumber];
    
        if (resultString.length > 3) {
            [resultString insertString:@"-" atIndex:3];
        }
    
        if (resultString.length > 6) {
            [resultString insertString:@"-" atIndex:6];
        }
    }

    if (newString.length > maxLocalNumberLength) {
    
        NSInteger villageCodeLength = MIN(newString.length - maxLocalNumberLength, maxVillageCodeLength);
    
        NSRange villageCodeRange = NSMakeRange(newString.length - maxLocalNumberLength - villageCodeLength, villageCodeLength);
    
        NSString* villageCode = [newString substringWithRange:villageCodeRange];
    
        villageCode = [NSString stringWithFormat:@"(%@) ", villageCode];
    
        [resultString insertString:villageCode atIndex:0];
    }

    if (newString.length > maxLocalNumberLength + maxVillageCodeLength) {
    
        NSInteger countryCodeLength = MIN(newString.length - maxLocalNumberLength - maxVillageCodeLength, maxCountryCodeLength);
    
        NSRange countryCodeRange = NSMakeRange(0, countryCodeLength);
    
        NSString* countryCode = [newString substringWithRange:countryCodeRange];
    
        countryCode = [NSString stringWithFormat:@"+%@ ", countryCode];
    
        [resultString insertString:countryCode atIndex:0];
    }

    textField.text = resultString;

    return NO;
}

-(BOOL) correctTrainingPeriod:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSMutableCharacterSet* validationSet = [NSMutableCharacterSet alphanumericCharacterSet];
    
    [validationSet addCharactersInString:@"."];
    
    [validationSet invert];
    
    NSArray* characters = [string componentsSeparatedByCharactersInSet:validationSet];
    
    NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    //Пропускает только допустимые символы и ограничение длины возраста
    if ([characters count] > 1 || newString.floatValue > 10.5f) {
        return NO;
    }
    
    //Ограничение ввода "."
    NSRange rangeInTextField = [textField.text rangeOfString:@"."];
    
    NSRange rangeInString = [string rangeOfString:@"."];
    
    if (rangeInTextField.location != NSNotFound && rangeInString.location != NSNotFound) {
        return NO;
    }
    
    return YES;
}

-(BOOL) correctLoginAndPassword:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSCharacterSet* validationSet = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    
    NSArray* characters = [string componentsSeparatedByCharactersInSet:validationSet];
    
    NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    //Пропускает только допустимые символы и ограничение длины
    if ([characters count] > 1 || newString.length > 24) {
        return NO;
    }
    
    return YES;
}

-(BOOL) correctEmail:(UITextField*)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSMutableCharacterSet* validationSet = [NSMutableCharacterSet alphanumericCharacterSet];
    
    [validationSet addCharactersInString:@"@._-"];
    
    [validationSet invert];
    
    NSArray* characters = [string componentsSeparatedByCharactersInSet:validationSet];
    
    NSString* newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    //Пропускает только допустимые символы и ограничение длины E-mail
    if ([characters count] > 1 || newString.length > 24) {
        return NO;
    }
    
    //Ограничение ввода "@"
    NSRange rangeInTextField = [textField.text rangeOfString:@"@"];
    
    NSRange rangeInString = [string rangeOfString:@"@"];
    
    if (rangeInTextField.location != NSNotFound && rangeInString.location != NSNotFound) {
        return NO;
    }
    
    return YES;
}


#pragma mark - Actions

- (IBAction)textChange:(UITextField *)sender {
    
    [self saveData];
}

- (IBAction)actionValueChange:(id)sender {
    
    if ([sender isEqual:self.ageSlider]) {
        
        NSInteger age = (int)self.ageSlider.value;
        
        NSString* ageInLabel = [NSString stringWithFormat:@"%@", [NSNumber numberWithInteger:age]];
        
        self.ageLabel.text = ageInLabel;
    }
    
    [self saveData];
}

@end
