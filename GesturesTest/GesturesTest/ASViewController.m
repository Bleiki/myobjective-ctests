//
//  ASViewController.m
//  GesturesTest
//
//  Created by Oleksii Skutarenko on 26.11.13.
//  Copyright (c) 2013 Alex Skutarenko. All rights reserved.
//

#import "ASViewController.h"

@interface ASViewController () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) UIView* testView;

@property (assign, nonatomic) CGFloat testViewScale;
@property (assign, nonatomic) CGFloat testViewRotation;

@end

@implementation ASViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.view.bounds) - 50,
                                                            CGRectGetMidY(self.view.bounds) - 50,
                                                            100, 100)];
    
    view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
                            UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    view.backgroundColor = [UIColor greenColor];
    
    [self.view addSubview:view];
    
    self.testView = view;
    
    UITapGestureRecognizer* tapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleTap:)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    
    UITapGestureRecognizer* doubleTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleDoubleTap:)];
    
    doubleTapGesture.numberOfTapsRequired = 2;//Колличество тапов на которые реагируют(на 1 уже не среагирует)
    
    [self.view addGestureRecognizer:doubleTapGesture];
    
    [tapGesture requireGestureRecognizerToFail:doubleTapGesture]; //одиночный тап не сработает, если сработает двойной(одиночный тап ждет какое то время)
    
    UITapGestureRecognizer* doubleTapDoubleTouchGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleDoubleTapDoubleTouch:)];
    
    doubleTapDoubleTouchGesture.numberOfTapsRequired = 2;
    doubleTapDoubleTouchGesture.numberOfTouchesRequired = 2;//Реагирует лишь на 2 тача.
    
    [self.view addGestureRecognizer:doubleTapDoubleTouchGesture];
    
    //UILongPressGestureRecognizer - долгое нажатие(стандартно 0.5с, но можно изменить)
    
    UIPinchGestureRecognizer* pinchGesture =
    [[UIPinchGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(handlePinch:)];//Зум, увеличение.
    pinchGesture.delegate = self;//Установка делегата (чтобы срабатывали оба метода в данном случае) Надо вверху объявить делегата.
    
    [self.view addGestureRecognizer:pinchGesture];
    
    UIRotationGestureRecognizer* rotationGesture =
    [[UIRotationGestureRecognizer alloc] initWithTarget:self
                                                 action:@selector(handleRotation:)];//Поворот
    rotationGesture.delegate = self;//Установка делегата (чтобы срабатывали оба метода в данном случае) Надо вверху объявить делегата.
    
    [self.view addGestureRecognizer:rotationGesture];
    
    
    UIPanGestureRecognizer* panGesture =
    [[UIPanGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handlePan:)];//Перемещение
    
    panGesture.delegate = self;
    
    [self.view addGestureRecognizer:panGesture];
    
    UISwipeGestureRecognizer* verticalSwipeGesture =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleVerticalSwipe:)];//Свайп
    
    verticalSwipeGesture.direction = UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp;//Маска указывает куда может делаться свайп(не различает верхний ли это свайп или нижний, надо создавать 2 разных свайпа)
    verticalSwipeGesture.delegate = self;
    
    [self.view addGestureRecognizer:verticalSwipeGesture];
    
    UISwipeGestureRecognizer* horizontalSwipeGesture =
    [[UISwipeGestureRecognizer alloc] initWithTarget:self
                                              action:@selector(handleHorizontalSwipe:)];//Свайп
    
    horizontalSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight;//Маска указывает куда может делаться свайп(не различает левый ли это свайп или правый, надо создавать 2 разных свайпа)
    horizontalSwipeGesture.delegate = self;
    
    [self.view addGestureRecognizer:horizontalSwipeGesture];
}


#pragma mark - Methods

- (UIColor*) randomColor {
    
    CGFloat r = (float)(arc4random() % 256) / 255.f;
    CGFloat g = (float)(arc4random() % 256) / 255.f;
    CGFloat b = (float)(arc4random() % 256) / 255.f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}

#pragma mark - Gestures

- (void) handleTap:(UITapGestureRecognizer*) tapGesture {
    
    NSLog(@"Tap: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
    
    self.testView.backgroundColor = [self randomColor];
}

- (void) handleDoubleTap:(UITapGestureRecognizer*) tapGesture {
    
    NSLog(@"Double Tap: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
    
    CGAffineTransform currentTransform = self.testView.transform;//постоянно увеличивается(сщхраняет предыдущие изменения)
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, 1.2f, 1.2f);//Увеличивает матрицу, которую ему дали в отличии от CGAffineTransformMakeScale.
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.testView.transform = newTransform;
                     }];
    
    self.testViewScale = 1.2f;//Запоминает предыдущее значение
}

- (void) handleDoubleTapDoubleTouch:(UITapGestureRecognizer*) tapGesture {
    
    NSLog(@"Double Tap Double Touch: %@", NSStringFromCGPoint([tapGesture locationInView:self.view]));
    
    CGAffineTransform currentTransform = self.testView.transform;//постоянно уменьшается(сохраняет предыдущие изменения)
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, 0.8f, 0.8f);//Уменьшает матрицу, которую ему дали в отличии от CGAffineTransformMakeScale.
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.testView.transform = newTransform;
                     }];
    
    self.testViewScale = 0.8f;//Запоминает предыдущее значение
}

- (void) handlePinch:(UIPinchGestureRecognizer*) pinchGesture {
    
    NSLog(@"handlePinch %1.3f", pinchGesture.scale);
    
    if (pinchGesture.state == UIGestureRecognizerStateBegan) {
        self.testViewScale = 1.f;
    }
    
    CGFloat newScale = 1.f + pinchGesture.scale - self.testViewScale;//Увеличиваем на разницу между тем что было и что стало(чтобы увеличение не росло в геометрической прогрессии(увеличили в 1.2, и готовое 1.2 еще в 1.2 раза)
    
    CGAffineTransform currentTransform = self.testView.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, newScale, newScale);
    
    self.testView.transform = newTransform;
    
    self.testViewScale = pinchGesture.scale;//Запоминает предыдущее значение
}

- (void) handleRotation:(UIRotationGestureRecognizer*) rotationGesture {
    
    NSLog(@"handleRotation %1.3f", rotationGesture.rotation);
    
    if (rotationGesture.state == UIGestureRecognizerStateBegan) {
        self.testViewRotation = 0;//Чтобы небыло рывка при повороте
    }
    
    CGFloat newRotation = rotationGesture.rotation - self.testViewRotation;//Для того, чтобы угол поворота не рос в геометрической прогресси.
    
    CGAffineTransform currentTransform = self.testView.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransform, newRotation);
    
    self.testView.transform = newTransform;
    
    self.testViewRotation = rotationGesture.rotation;
}

- (void) handlePan:(UIPanGestureRecognizer*) panGesture {
    
    NSLog(@"handlePan");
    
    self.testView.center = [panGesture locationInView:self.view];
    
}

- (void) handleVerticalSwipe:(UISwipeGestureRecognizer*) swipeGesture {
    
    NSLog(@"Vertical Swipe");
    
}

- (void) handleHorizontalSwipe:(UISwipeGestureRecognizer*) swipeGesture {
    
    NSLog(@"Horizontal Swipe");
    
}

#pragma mark - UIGestureRecognizerDelegate
//Нужно ли запускать 2 Gesture одновременно?
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
            shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

//Pan не сработает, если сработает Swipe(Pan ждет какое то время)
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]] && [otherGestureRecognizer isKindOfClass:[UISwipeGestureRecognizer class]];
}

@end
