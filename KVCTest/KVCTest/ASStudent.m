//
//  ASStudent.m
//  KVCTest
//
//  Created by Oleksii Skutarenko on 25.01.14.
//  Copyright (c) 2014 Alex Skutarenko. All rights reserved.
//

#import "ASStudent.h"

@implementation ASStudent


- (void) setName:(NSString *)name {
    
    _name = name;
    
    NSLog(@"Student setName: %@", name);
}


- (void) setAge:(NSInteger)age {
    
    _age = age;
    
    NSLog(@"Student setAge: %d", age);
}

//Cрабатывает при распечатке объекта
- (NSString*) description {
    return [NSString stringWithFormat:@"Student: %@ %d", self.name, self.age];
}

//Не забыть про супер, чтобы установилось свойство
- (void) setValue:(id)value forKey:(NSString *)key {
    
    NSLog(@"Student setValue:%@ forKey:%@", value, key);
    
    [super setValue:value forKey:key];
}

//Перелпределение, чтобы приложение не упало в случае неверного ключа
- (void) setValue:(id)value forUndefinedKey:(NSString *)key {
    
    NSLog(@"setValueForUndefinedKey");
}

//Перелпределение, чтобы приложение не упало в случае неверного ключа
- (id) valueForUndefinedKey:(NSString *)key {
    NSLog(@"valueForUndefinedKey");
    return nil;
}

- (void) changeName {
    //В этом случае обсервер узнает об изменении свойства, благодаря двум методам(без этих методов через айвар он ни о чем не узнает) (сетер не вызовется)  Это позоляет оповещать обсервер!!!!
    [self willChangeValueForKey:@"name"];
    _name = @"FakeName";
    [self didChangeValueForKey:@"name"];
}

/*
 //Можно ли установить объекту по ключу такое значение (общий метод)
- (BOOL)validateValue:(inout id *)ioValue forKey:(NSString *)inKey error:(out NSError **)outError {
    
    if ([inKey isEqualToString:@"name"]) {
        
        NSString* newName = *ioValue;
        
        if (![newName isKindOfClass:[NSString class]]) {
            *outError = [[NSError alloc] initWithDomain:@"Not NSString" code:123 userInfo:nil];
            return NO;
        }
        
        if ([newName rangeOfString:@"1"].location != NSNotFound) {
            *outError = [[NSError alloc] initWithDomain:@"Has numbers" code:324 userInfo:nil];
            return NO;
        }
    }
    
    return YES;
}*/

/*
//Ддя свойства нейм устанавливается сразу такой валидатор (его личный метод) общего метода не должно быть
- (BOOL) validateName:(inout id *)ioValue error:(out NSError **)outError {
    
    NSLog(@"AAAAAA");
    
    return YES;
}
*/
@end
