//
//  ViewController.m
//  OutletsTest
//
//  Created by Admin on 06.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIColor*) randomColor {
    
    CGFloat r = (float)(arc4random() % 256) / 255;
    CGFloat g = (float)(arc4random() % 256) / 255;
    CGFloat b = (float)(arc4random() % 256) / 255;

    return [UIColor colorWithRed:r green:g blue:b alpha:1];
}


-(void) viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [super viewWillTransitionToSize:(CGSize)size
          withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator];
    for (UIView* v in self.testViews) {
        v.backgroundColor = [self randomColor];
    }
    
    //self.testView.backgroundColor = [self randomColor];
    //self.testView2.backgroundColor = [self randomColor];

}

@end
