//
//  ViewController.m
//  GeometryHomeWork
//
//  Created by Admin on 05.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(strong, nonatomic) UIView* testView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect board;
    board.size.height = MIN(self.view.bounds.size.width , self.view.bounds.size.height);
    board.size.width = MIN(self.view.bounds.size.width , self.view.bounds.size.height);
    board.origin.x = (self.view.bounds.size.width - board.size.width) / 2;
    board.origin.y = (self.view.bounds.size.height - board.size.height) / 2;

    self.testView = [self newSquare:board andBackgroundColor:[UIColor grayColor] andParentView:self.view];

    for (NSInteger i = 0; i < 8; i++){
        for (NSInteger j = 0; j < 4; j++) {
            
            CGRect bigSquare;
            bigSquare.size.height = board.size.height / 8;
            bigSquare.size.width = board.size.width / 8;
            bigSquare.origin.x = 0 + (board.size.width / 4) * j;
            bigSquare.origin.y = 0 + (board.size.height / 8) * i;
            
            if (!(i % 2)) {
                
                bigSquare.origin.x += board.size.width / 8;
            }
            
            [self newSquare:bigSquare andBackgroundColor:[UIColor blackColor] andParentView:self.testView];
            
            if (i < 3) {
                
                CGRect miniSquare;
                miniSquare.size.height = bigSquare.size.height / 2;
                miniSquare.size.width = bigSquare.size.width / 2;
                miniSquare.origin.x = (bigSquare.size.width / 4) + (bigSquare.size.width * 2) * j;
                miniSquare.origin.y = (bigSquare.size.height / 4) + (bigSquare.size.height) * i;
                
                if (!(i % 2)) {
                    
                    miniSquare.origin.x += bigSquare.size.width;
                }
                
                [self newSquare:miniSquare andBackgroundColor:[UIColor redColor] andParentView:self.testView];
                
            }
            
            if (i > 4 & i < 8) {
                
                CGRect miniSquare;
                miniSquare.size.height = bigSquare.size.height / 2;
                miniSquare.size.width = bigSquare.size.width / 2;
                miniSquare.origin.x = (bigSquare.size.width / 4) + (bigSquare.size.width * 2) * j;
                miniSquare.origin.y = (bigSquare.size.height / 4) + (bigSquare.size.height) * i;
                
                if (!(i % 2)) {
                    
                    miniSquare.origin.x += bigSquare.size.width;
                }
                
                [self newSquare:miniSquare andBackgroundColor:[UIColor whiteColor] andParentView:self.testView];
                
            }
        }
    }
 }



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [super viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator];
    
    for (UIView* view in self.testView.subviews) {
        
        UIColor* bigSquareColor = [self randomColor];
        
        if () {
            
            view.backgroundColor = bigSquareColor;
        }
    }
    
    for (UIView* view in self.testView.subviews) {
        
        if ([view.backgroundColor isEqual:[UIColor redColor]] | [view.backgroundColor isEqual:[UIColor whiteColor]]) {
            
            
        }
    }
}

-(UIColor*) randomColor {
    
    CGFloat r = (float)(arc4random() % 256) / 255;
    CGFloat g = (float)(arc4random() % 256) / 255;
    CGFloat b = (float)(arc4random() % 256) / 255;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1];

}

-(UIView*) newSquare:(CGRect) rect andBackgroundColor:(UIColor*) color andParentView:(UIView*) parentView
              andTag:(NSInteger) tag {
    
    UIView* squareView = [[UIView alloc] initWithFrame:rect];
    squareView.backgroundColor = color;
    squareView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    [squareView tag] = tag;
    [parentView addSubview:squareView];
    
    return squareView;
}

@end
