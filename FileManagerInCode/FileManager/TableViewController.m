//
//  TableViewController.m
//  FileManager
//
//  Created by Admin on 04.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()

@property (strong, nonatomic) NSString* path;
@property (strong, nonatomic) NSArray* contents;

@end

@implementation TableViewController

#pragma mark - StandartMethods

-(id) initWithFolderPath:(NSString*) path {
    
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        self.path = path;
        
        NSError* error = nil;
        
        self.contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:self.path error:&error];
        
        NSMutableArray* folders = [NSMutableArray array];
        
        NSMutableArray* files = [NSMutableArray array];
        
        for (NSString* content in self.contents) {
            
            NSInteger index = [self.contents indexOfObject:content];
            
            if ([self isFolderOrFile:index]) {
                [folders addObject:content];
            } else {
                [files addObject:content];
            }
        }
        
        NSArray* sortedFolders = [folders sortedArrayUsingComparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            return [obj1 compare:obj2];
        }];
        
        NSArray* sortedFiles = [files sortedArrayUsingComparator:^NSComparisonResult(NSString* obj1, NSString* obj2) {
            return [obj1 compare:obj2];
        }];
        
        NSMutableArray* resultArray = [NSMutableArray arrayWithArray:sortedFolders];
        
        [resultArray addObjectsFromArray:sortedFiles];
        
        self.contents = resultArray;
        
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = [self.path lastPathComponent];
    
    if ([self.navigationController.viewControllers count] > 1) {
        UIBarButtonItem* item = [[UIBarButtonItem alloc] initWithTitle:@"Back to Root"
                                                                 style:UIBarButtonItemStylePlain
                                                                 target:self
                                                                 action:@selector(backToRoot:)];
        
        self.navigationItem.rightBarButtonItem = item;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - MyMethods

-(BOOL) isDirectory:(NSIndexPath*) indexPath {
    
    NSString* fileName = [self.contents objectAtIndex:indexPath.row];
    
    NSString* filePath = [self.path stringByAppendingPathComponent:fileName];
    
    BOOL isDirectory = NO;
    
    [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
    
    return isDirectory;
}

-(BOOL) isFolderOrFile:(NSInteger) i {
    
    NSString* fileName = [self.contents objectAtIndex:i];
    
    NSString* filePath = [self.path stringByAppendingPathComponent:fileName];
    
    BOOL isDirectory = NO;
    
    [[NSFileManager defaultManager] fileExistsAtPath:filePath isDirectory:&isDirectory];
    
    return isDirectory;
}

- (NSString*) fileSizeFromValue:(unsigned long long) size {
    
    static NSString* units[] = {@"B", @"KB", @"MB", @"GB", @"TB"};
    static int unitsCount = 5;
    
    int index = 0;
    
    double fileSize = (double)size;
    
    while (fileSize > 1024 && index < unitsCount) {
        fileSize /= 1024;
        index++;
    }
    
    return [NSString stringWithFormat:@"%.2f %@", fileSize, units[index]];
}


#pragma mark - Actions

-(void) backToRoot:(UIBarButtonItem*) sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contents count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifire = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifire];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifire];
    }
    
    NSString* fileName = [self.contents objectAtIndex:indexPath.row];
    
    cell.textLabel.text = fileName;
    
    NSString* path = [self.path stringByAppendingPathComponent:fileName];
    
    NSDictionary* atributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
    
    if ([self isDirectory:indexPath]) {
        cell.imageView.image = [UIImage imageNamed:@"folder"];
        cell.detailTextLabel.text = nil;
    } else {
        cell.imageView.image = [UIImage imageNamed:@"file"];
        cell.detailTextLabel.text = [self fileSizeFromValue:[atributes fileSize]];
    }
    
    return cell;
    
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self isDirectory:indexPath]) {
        
        NSString* fileName = [self.contents objectAtIndex:indexPath.row];
        
        NSString* path = [self.path stringByAppendingPathComponent:fileName];
        
        TableViewController* vc = [[TableViewController alloc] initWithFolderPath:path];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}


@end
