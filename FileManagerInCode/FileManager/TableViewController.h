//
//  TableViewController.h
//  FileManager
//
//  Created by Admin on 04.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController

-(id) initWithFolderPath:(NSString*) path;

@end
