//
//  ViewController.m
//  TableViewSearch
//
//  Created by Admin on 05.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"
#import "VGSection.h"
#import "VGStudent.h"

@interface ViewController ()

@property (strong, nonatomic) NSArray* arrayOfStudents;
@property (strong, nonatomic) NSArray* arrayOfSections;

@end

typedef enum {
    
    VGSegmentControlStateSortDate,
    VGSegmentControlStateSortFirstName,
    VGSegmentControlStateSortLastName
    
}VGSegmentControlState;

@implementation ViewController

#pragma mark - LoadMethods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Создаю студентов
    NSMutableArray* students = [NSMutableArray array];
    
    for (NSInteger i = 0; i < arc4random() % 101 + 200; i++) {
        
        VGStudent* student = [VGStudent randomStudent];
        
        [students addObject:student];
    }
    
    //Сортирую студентов
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"MM"];
            
    [students sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
                
        NSString* str1 = [df stringFromDate:[obj1 birthday]];
        NSString* str2 = [df stringFromDate:[obj2 birthday]];
        return [str1 compare:str2];
    }];
        
    self.arrayOfStudents = students;

    [self sortStudentsAtSections:self.arrayOfStudents withFilter:self.searchBar.text];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - MyMethods

-(void) sortStudentsAtSections:(NSArray*) array withFilter:(NSString*) filterString {
    
    //Разбиваю студентов на секции
    NSMutableArray* sections = [NSMutableArray array];
    
    NSInteger currentMonth = 0;
    
    for (VGStudent* student in self.arrayOfStudents) {
        
        if ([filterString length] > 0 &&
            [student.firstName rangeOfString:filterString].location == NSNotFound &&
            [student.lastName rangeOfString:filterString].location == NSNotFound)  {
            
            continue;
        }
        
        NSCalendar* calendar = [NSCalendar currentCalendar];
        
        NSDateComponents* dateComponents = [calendar components:NSCalendarUnitMonth fromDate:student.birthday];
        
        NSInteger numberOfMonth = [dateComponents month];
        
        VGSection* section = nil;
        
        if (numberOfMonth != currentMonth) {
            
            section = [[VGSection alloc] init];
            
            [sections addObject:section];
            
            section.students = [[NSMutableArray alloc] init];
            
            NSDateFormatter* df = [[NSDateFormatter alloc] init];
            
            [df setDateFormat:@"MMMM"];
            
            section.name = [df stringFromDate:student.birthday];
            
            currentMonth = numberOfMonth;
            
        } else {
            
            section = [sections lastObject];
        }
        
        [section.students addObject:student];
    }
    
    //Cортирую студентов в секции по имени и фамилии
    for (VGSection* section in sections) {
        
        NSSortDescriptor* sortFirstName = [[NSSortDescriptor alloc] initWithKey:@"firstName" ascending:YES];
        
        NSSortDescriptor* sortLastName = [[NSSortDescriptor alloc] initWithKey:@"lastName" ascending:YES];
        
        [section.students sortUsingDescriptors:@[sortFirstName, sortLastName]];
    }
    
    self.arrayOfSections = sections;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.arrayOfSections count];
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    VGSection* sec = [self.arrayOfSections objectAtIndex:section];
    
    return sec.name;
}

- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView  {
    
    NSDateFormatter* df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"MMM"];
    
    NSMutableArray* arrayOfIndex = [NSMutableArray array];
    
    for (VGSection* section in self.arrayOfSections) {

        VGStudent* student = [section.students lastObject];
        
        NSString* date = [df stringFromDate:student.birthday];
        
        [arrayOfIndex addObject:date];
    }
    
    NSArray* array = [NSArray arrayWithArray:arrayOfIndex];
    
    [arrayOfIndex removeAllObjects];
    
    return array;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    VGSection* sec = [self.arrayOfSections objectAtIndex:section];
    
    return [sec.students count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString* identifire = @"Cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifire];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifire];
    }
    
    VGSection* section = [self.arrayOfSections objectAtIndex:indexPath.section];
    
    VGStudent* student = [section.students objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd.MM.yyyy"];
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:student.birthday]];
    
    return cell;
}


#pragma mark - UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [searchBar setShowsCancelButton:YES animated:YES];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
    
    [searchBar setShowsCancelButton:NO animated:YES];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    NSLog(@"textDidChange: %@", searchText);
    
    [self sortStudentsAtSections:self.arrayOfStudents withFilter:searchText];
    
    [self.tableView reloadData];
}


@end
