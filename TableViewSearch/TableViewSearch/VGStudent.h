//
//  VGStudent.h
//  TableViewSearch
//
//  Created by Admin on 05.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VGStudent : NSObject

@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSDate* birthday;
 
+(VGStudent*) randomStudent;

@end
