//
//  VGStudent.m
//  TableViewSearch
//
//  Created by Admin on 05.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import "VGStudent.h"

@implementation VGStudent

+(VGStudent*) randomStudent {
    
    NSArray* firstNames = [NSArray arrayWithObjects:
                           @"Tran", @"Lenore", @"Bud", @"Fredda", @"Katrice",
                           @"Clyde", @"Hildegard", @"Vernell", @"Nellie", @"Rupert",
                           @"Billie", @"Tamica", @"Crystle", @"Kandi", @"Caridad",
                           @"Vanetta", @"Taylor", @"Pinkie", @"Ben", @"Rosanna",
                           @"Eufemia", @"Britteny", @"Ramon", @"Jacque", @"Telma",
                           @"Colton", @"Monte", @"Pam", @"Tracy", @"Tresa",
                           @"Willard", @"Mireille", @"Roma", @"Elise", @"Trang",
                           @"Ty", @"Pierre", @"Floyd", @"Savanna", @"Arvilla",
                           @"Whitney", @"Denver", @"Norbert", @"Meghan", @"Tandra",
                           @"Jenise", @"Brent", @"Elenor", @"Sha", @"Jessie", nil];;
    
    NSArray* lastNames = [NSArray arrayWithObjects:
                          
                          @"Farrah", @"Laviolette", @"Heal", @"Sechrest", @"Roots",
                          @"Homan", @"Starns", @"Oldham", @"Yocum", @"Mancia",
                          @"Prill", @"Lush", @"Piedra", @"Castenada", @"Warnock",
                          @"Vanderlinden", @"Simms", @"Gilroy", @"Brann", @"Bodden",
                          @"Lenz", @"Gildersleeve", @"Wimbish", @"Bello", @"Beachy",
                          @"Jurado", @"William", @"Beaupre", @"Dyal", @"Doiron",
                          @"Plourde", @"Bator", @"Krause", @"Odriscoll", @"Corby",
                          @"Waltman", @"Michaud", @"Kobayashi", @"Sherrick", @"Woolfolk",
                          @"Holladay", @"Hornback", @"Moler", @"Bowles", @"Libbey",
                          @"Spano", @"Folson", @"Arguelles", @"Burke", @"Rook", nil];
    
    
    VGStudent* student = [[VGStudent alloc] init];
    
    student.firstName = [firstNames objectAtIndex:arc4random() % 50];
    student.lastName = [lastNames objectAtIndex:arc4random() % 50];
    
    NSInteger year = arc4random() % 51 + 1951;
    
    NSInteger month = arc4random() % 12 + 1;
    
    NSInteger day = 0;
    
    if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) {
        day = arc4random() % 31 + 1;
    } else if (month == 4 || month == 6 || month == 9 || month == 11) {
         day = arc4random() % 30 + 1;
    } else {
        if ((year % 4 == 0 && year % 100 != 0) || year % 400 ==0) {
            day = arc4random() % 29 + 1;
        } else {
            day = arc4random() % 28 + 1;
        }
    }
    
    NSDate* date = [NSDate date];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* dateComponents = [calendar components:NSCalendarUnitYear fromDate:date];
    
    [dateComponents setDay:day];
    
    [dateComponents setMonth:month];
    
    [dateComponents setYear:year];
                       
    student.birthday = [calendar dateFromComponents:dateComponents];
        
    return student;
}

@end
