//
//  VGSection.h
//  TableViewSearch
//
//  Created by Admin on 05.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VGSection : NSObject

@property (strong, nonatomic) NSString* name;
@property (strong, nonatomic) NSMutableArray* students;

@end
