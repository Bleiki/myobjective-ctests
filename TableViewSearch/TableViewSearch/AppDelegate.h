//
//  AppDelegate.h
//  TableViewSearch
//
//  Created by Admin on 05.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

