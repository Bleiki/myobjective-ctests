//
//  main.m
//  KVCHomeWork
//
//  Created by Admin on 07.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
