//
//  VGStudent.h
//  KVCHomeWork
//
//  Created by Admin on 07.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//
//Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer/SDKs/iPhoneOS10.1.sdk/System/Library/Frameworks/Foundation.framework/Headers/NSByteOrder.h
#import <Foundation/Foundation.h>

@interface VGStudent : NSObject

@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (strong, nonatomic) NSDate* birthday;
@property (strong, nonatomic) NSString* gender;
@property (assign, nonatomic) float grade;

@end
