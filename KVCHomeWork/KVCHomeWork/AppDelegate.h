//
//  AppDelegate.h
//  KVCHomeWork
//
//  Created by Admin on 07.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

