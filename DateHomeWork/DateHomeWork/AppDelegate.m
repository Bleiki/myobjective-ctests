//
//  AppDelegate.m
//  DateHomeWork
//
//  Created by Admin on 03.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "AppDelegate.h"
#import "VGStudent.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"dd.MM.yyyy"];
    
    
    //Create array of students with date of birth
    NSMutableArray* arrayOfStudents = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 30; i++) {
        
        VGStudent* student = [[VGStudent alloc] init];
        
        NSCalendar* calendar = [NSCalendar currentCalendar];
        
        NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
        
        [components setYear:[components year] - (arc4random() % 35 + 16)];
        
        [components setMonth:arc4random() % 12 + 1];
        
        if ([components month] == 02) {
            
            [components setDay:arc4random() % 28 + 1];
           
        } else {
            
            [components setDay:arc4random() % 31 + 1];
        }
        
        student.dateOfBirth = [calendar dateFromComponents:components];
        
        [arrayOfStudents addObject:student];
    }
    
    /*
    for (VGStudent* student in arrayOfStudents) {
        
        NSLog(@"Date of birth: %@", [formatter stringFromDate:(student.dateOfBirth)]);
    }
    */
    
    
    //Create of sorted array from students with name and lastName
    NSArray* sortedArrayOfStudents = [arrayOfStudents sortedArrayUsingComparator:^(id obj1, id obj2) {
        
        return [[obj1 dateOfBirth] compare:[obj2 dateOfBirth]];
    }];
    
    NSArray* arrayOfNames = [NSArray arrayWithObjects:@"Slava", @"Vika", @"Olya", @"Mark", @"Misha", @"Vlad", @"Lena", @"Igor", nil];
    
    NSArray* arrayOfLastNames = [NSArray arrayWithObjects:@"Osipov", @"Karnaukhova", @"Gerasimenok", @"Mavrin", @"Ivanov", @"Sidorov", @"Pavlov", @"Sedunov", nil];
    
    for (VGStudent* student in sortedArrayOfStudents) {
        
        student.name = [arrayOfNames objectAtIndex:arc4random() % 8];
        
        student.lastName = [arrayOfLastNames objectAtIndex:arc4random() % 8];
        
        NSLog(@"Name: %@. LastName: %@. Date of birth: %@.", student.name, student.lastName, [formatter stringFromDate:(student.dateOfBirth)]);
    }
    
    
    //Age difference
    NSCalendar* calendar = [NSCalendar currentCalendar];
        
    NSDateComponents* components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay
                fromDate:[[sortedArrayOfStudents objectAtIndex:0] dateOfBirth]
                toDate:[[sortedArrayOfStudents objectAtIndex:([sortedArrayOfStudents count] - 1)]  dateOfBirth]
                options:0];
    
    NSLog(@"Difference in age = %@", components);
    
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
