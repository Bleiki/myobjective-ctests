//
//  VGStudent.h
//  DateHomeWork
//
//  Created by Admin on 03.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VGStudent : NSObject

@property(assign, nonatomic) NSDate* dateOfBirth;
@property(strong, nonatomic) NSString* name;
@property(strong, nonatomic) NSString* lastName;


@end
