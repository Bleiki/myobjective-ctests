//
//  VGStudent.h
//  TableEditingHomeWork
//
//  Created by Admin on 31.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VGStudent : NSObject

@property (strong, nonatomic) NSString* firstName;
@property (strong, nonatomic) NSString* lastName;
@property (assign, nonatomic) float averageMark;

+(VGStudent*) randomStudent;

@end
