//
//  ViewController.m
//  TableEditingHomeWork
//
//  Created by Admin on 31.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"
#import "VGStudent.h"
#import "VGGroup.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UITableView* tableView;
@property (strong, nonatomic) NSMutableArray* arrayOfGroups;

@end

@implementation ViewController

#pragma mark - View Methods

//Создание tableView
-(void) loadView {
    [super loadView];
        
    CGRect frame = self.view.bounds;
    
    frame.origin = CGPointZero;
    
    UITableView* tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    
    tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    tableView.delegate = self;
    
    tableView.dataSource = self;
        
    [self.view addSubview:tableView];
    
    self.tableView = tableView;
    
    self.tableView.allowsSelectionDuringEditing = NO;
}

//Создание групп, студентов и кнопок на NavigationBar
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayOfGroups = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 5; i++) {
        
        VGGroup* group = [[VGGroup alloc] init];
        
        group.name = [NSString stringWithFormat:@"Group number %li", i + 1];
        
        NSMutableArray* array = [NSMutableArray array];
        
        for (NSInteger j = 0; j < 10; j++) {
            
            VGStudent* student = [VGStudent randomStudent];
            
            [array addObject:student];
        }
        
        group.students = array;
        
        [self.arrayOfGroups addObject:group];
    }
    
    [self.tableView reloadData];
    
    self.navigationItem.title = @"Students";
    
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit
                                                                                target:self
                                                                                action:@selector(actionEdit:)];
    
    self.navigationItem.rightBarButtonItem = editButton;
    
    UIBarButtonItem* addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(actionAdd:)];
    
    self.navigationItem.leftBarButtonItem = addButton;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - My Methods

//Сдвиг индекса массива из-за ряда доюавления нового студента
-(NSInteger) indexOfStudentForRow:(NSInteger) row {
    return row - 1;
}


#pragma mark - Actions

//Вызывается при нажатии кнопки редактирования
-(void) actionEdit:(UIBarButtonItem*) sender {
    
    BOOL isEditing = self.tableView.editing;
    
    [self.tableView setEditing:!isEditing animated:YES];
    
    UIBarButtonSystemItem item = UIBarButtonSystemItemEdit;
    
    if (self.tableView.editing) {
        item = UIBarButtonSystemItemDone;
    }
    
    UIBarButtonItem* editButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:item
                                                                                target:self
                                                                                action:@selector(actionEdit:)];
    
    [self.navigationItem setRightBarButtonItem:editButton animated:YES];
}

//Вызывается при нажатии кнопки добавления
-(void) actionAdd:(UIBarButtonItem*) sender {
    
    VGGroup* group = [[VGGroup alloc] init];
    group.name = [NSString stringWithFormat:@"Group number %li", [self.arrayOfGroups count] + 1];
    
    NSInteger indexOfInsert = 0;
    
    [self.arrayOfGroups insertObject:group atIndex:indexOfInsert];

    [self.tableView beginUpdates];
    
    NSIndexSet* indexSet = [NSIndexSet indexSetWithIndex:indexOfInsert];
    
    UITableViewRowAnimation animation = UITableViewRowAnimationFade;
    
    if ([self.arrayOfGroups count] > 1) {
        animation = UITableViewRowAnimationLeft;
    }
    
    [self.tableView insertSections:indexSet withRowAnimation:animation];
    
    [self.tableView endUpdates];
    
    //Задержка
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        }
    });
}


#pragma mark - UITableViewDataSource

//Определение числа секций
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.arrayOfGroups count];
}

//Название секции
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [[self.arrayOfGroups objectAtIndex:section] name];
}

//Число рядов в секции
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    VGGroup* group = [self.arrayOfGroups objectAtIndex:section];
    
    return [group.students count] + 1;
    
}

//Создание ячеек для секций и ряда добавления студентов
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        static NSString* addStudentIdentifire = @"Add Student";
        
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:addStudentIdentifire];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addStudentIdentifire];
        }
        
        cell.textLabel.text = @"Add Student";
        cell.textLabel.textColor = [UIColor blueColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        
        return cell;
        
    } else {
        
        static NSString* studentIdentifier = @"Student";
        
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:studentIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:studentIdentifier];
        }
        
        VGGroup* group = [self.arrayOfGroups objectAtIndex:indexPath.section];
        
        VGStudent* student = [group.students objectAtIndex:[self indexOfStudentForRow:indexPath.row]];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
        
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%1.2f", student.averageMark];
        
        if (student.averageMark < 4) {
            cell.detailTextLabel.textColor = [UIColor redColor];
        } else {
            cell.detailTextLabel.textColor = [UIColor greenColor];
        }
        
        return cell;
    }

}

//Разрешение перемещать ячейки (запрет на перемещение 0-й ячейки)
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return (indexPath.row > 0);
}

//Вызывается при перемещении ячейки
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    VGGroup* sourceGroup = [self.arrayOfGroups objectAtIndex:sourceIndexPath.section];
    VGGroup* destinationGroup = [self.arrayOfGroups objectAtIndex:destinationIndexPath.section];
    
    VGStudent* student = [sourceGroup.students objectAtIndex:[self indexOfStudentForRow:sourceIndexPath.row]];
    
    NSMutableArray* sourceTemporaryArray = [NSMutableArray arrayWithArray:sourceGroup.students];
    NSMutableArray* destinationTemporaryArray = [NSMutableArray arrayWithArray:destinationGroup.students];

    if ([sourceTemporaryArray isEqual:destinationTemporaryArray]) {
        
        [sourceTemporaryArray removeObjectAtIndex:[self indexOfStudentForRow:sourceIndexPath.row]];
        
        [sourceTemporaryArray insertObject:student atIndex:[self indexOfStudentForRow:destinationIndexPath.row]];
        
        sourceGroup.students = sourceTemporaryArray;
        
    } else {
    
        [sourceTemporaryArray removeObject:student];
    
        [destinationTemporaryArray insertObject:student atIndex:[self indexOfStudentForRow:destinationIndexPath.row]];
    
        sourceGroup.students = sourceTemporaryArray;
    
        destinationGroup.students = destinationTemporaryArray;
    }
}

//ВЫзывается при удалении ячейки
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        VGGroup* group = [self.arrayOfGroups objectAtIndex:indexPath.section];
        
        VGStudent* student = [group.students objectAtIndex:[self indexOfStudentForRow:indexPath.row]];
        
        NSMutableArray* tempArray = [NSMutableArray arrayWithArray:group.students];
        
        [tempArray removeObject:student];
        
        group.students = tempArray;
        
        [tableView beginUpdates];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        
        [tableView endUpdates];
    }
}

#pragma mark - UITableViewDelegate

//Что будет слева от ячейки при переходе в режим редактирования
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.row == 0 ? UITableViewCellEditingStyleNone : UITableViewCellEditingStyleDelete;
}

//Можно ли выбирать ячейки во время редактирования
- (BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

//Куда переместить ячейку при ее перемещении (чтобы не вставала в 0-й ряд)
- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    
    if (proposedDestinationIndexPath.row == 0) {
        return sourceIndexPath;
    } else {
        return proposedDestinationIndexPath;
    }
}

//Вызывается при выборе ячейки
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//Отмена выбора ряда
    
    if (indexPath.row == 0) {
        
        VGGroup* group = [self.arrayOfGroups objectAtIndex:indexPath.section];
        
        NSMutableArray* tempArray = nil;
        
        if (group.students) {
            tempArray = [NSMutableArray arrayWithArray:group.students];
        } else {
            tempArray = [NSMutableArray array];
        }
        
        VGStudent* student = [VGStudent randomStudent];
        
        NSInteger newStudentIndex = 0;
        
        [tempArray insertObject:student atIndex:newStudentIndex];
        
        group.students = tempArray;
        
        [tableView beginUpdates];
        
        NSIndexPath* index = [NSIndexPath indexPathForItem:newStudentIndex + 1 inSection:indexPath.section];
        
        [tableView insertRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationLeft];
        
        [tableView endUpdates];
        
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            }
        });
    }
}


@end
