//
//  VGGroup.h
//  TableEditingHomeWork
//
//  Created by Admin on 02.01.17.
//  Copyright © 2017 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VGGroup : NSObject

@property (strong, nonatomic) NSArray* students;
@property (strong, nonatomic) NSString* name;
@end
