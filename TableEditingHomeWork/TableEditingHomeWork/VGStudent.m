//
//  VGStudent.m
//  TableEditingHomeWork
//
//  Created by Admin on 31.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "VGStudent.h"

@implementation VGStudent

+(VGStudent*) randomStudent {
    
    NSArray* firstNames = [NSArray arrayWithObjects:
        @"Tran", @"Lenore", @"Bud", @"Fredda", @"Katrice",
        @"Clyde", @"Hildegard", @"Vernell", @"Nellie", @"Rupert",
        @"Billie", @"Tamica", @"Crystle", @"Kandi", @"Caridad",
        @"Vanetta", @"Taylor", @"Pinkie", @"Ben", @"Rosanna",
        @"Eufemia", @"Britteny", @"Ramon", @"Jacque", @"Telma",
        @"Colton", @"Monte", @"Pam", @"Tracy", @"Tresa",
        @"Willard", @"Mireille", @"Roma", @"Elise", @"Trang",
        @"Ty", @"Pierre", @"Floyd", @"Savanna", @"Arvilla",
        @"Whitney", @"Denver", @"Norbert", @"Meghan", @"Tandra",
        @"Jenise", @"Brent", @"Elenor", @"Sha", @"Jessie", nil];;
    
    NSArray* lastNames = [NSArray arrayWithObjects:
        
        @"Farrah", @"Laviolette", @"Heal", @"Sechrest", @"Roots",
        @"Homan", @"Starns", @"Oldham", @"Yocum", @"Mancia",
        @"Prill", @"Lush", @"Piedra", @"Castenada", @"Warnock",
        @"Vanderlinden", @"Simms", @"Gilroy", @"Brann", @"Bodden",
        @"Lenz", @"Gildersleeve", @"Wimbish", @"Bello", @"Beachy",
        @"Jurado", @"William", @"Beaupre", @"Dyal", @"Doiron",
        @"Plourde", @"Bator", @"Krause", @"Odriscoll", @"Corby",
        @"Waltman", @"Michaud", @"Kobayashi", @"Sherrick", @"Woolfolk",
        @"Holladay", @"Hornback", @"Moler", @"Bowles", @"Libbey",
        @"Spano", @"Folson", @"Arguelles", @"Burke", @"Rook", nil];

    
    VGStudent* student = [[VGStudent alloc] init];
    
    student.firstName = [firstNames objectAtIndex:arc4random() % 50];
    student.lastName = [lastNames objectAtIndex:arc4random() % 50];
    student.averageMark = ((float)((arc4random() % 301) + 200)) / 100;
    
    return student;
}

@end
