//
//  VGStudent.h
//  ThreadHomeWork
//
//  Created by Admin on 01.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VGStudent : NSObject

@property(strong, nonatomic)NSString* name;

-(void) guesstheAnswer:(NSInteger) number and:(NSInteger) minimum  and:(NSInteger) maximum;

@end
