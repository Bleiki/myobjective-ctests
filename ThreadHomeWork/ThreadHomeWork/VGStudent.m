//
//  VGStudent.m
//  ThreadHomeWork
//
//  Created by Admin on 01.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "VGStudent.h"

@implementation VGStudent

-(void) guesstheAnswer:(NSInteger) number and:(NSInteger) minimum  and:(NSInteger) maximum {
    
    dispatch_queue_t queue =  dispatch_queue_create("com.threadhomework.v.gerasimenok", DISPATCH_QUEUE_SERIAL);
    dispatch_async(queue, ^{
        
        NSInteger answer = arc4random() %(maximum - minimum) + minimum;
        
        NSLog(@"Student %@ began to guess the answer", self.name);
        
        @synchronized (self) {
            
            while (answer != number) {
                
                NSLog(@"Student %@ don't guess the answer (%li)", self.name, answer);
                
                answer = arc4random() %(maximum - minimum) + minimum;
            }
            NSLog(@"%@, Answer: %li.",self.name ,answer);
        }

    });
}



@end
