//
//  AppDelegate.m
//  ThreadHomeWork
//
//  Created by Admin on 01.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "AppDelegate.h"
#import "VGStudent.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    VGStudent* student1 = [[VGStudent alloc] init];
    student1.name = @"Slava";
    
    VGStudent* student2 = [[VGStudent alloc] init];
    student2.name = @"Petya";
    
    VGStudent* student3 = [[VGStudent alloc] init];
    student3.name = @"Vika";
    
    VGStudent* student4 = [[VGStudent alloc] init];
    student4.name = @"Olya";
    
    VGStudent* student5 = [[VGStudent alloc] init];
    student5.name = @"Pasha";
    
    NSArray* arrayOfStudents = [NSArray arrayWithObjects:student1, student2, student3, student4, student5, nil];
    
    for (VGStudent* student in arrayOfStudents) {
        [student guesstheAnswer:53 and:20 and:100];
    }
    
    
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
