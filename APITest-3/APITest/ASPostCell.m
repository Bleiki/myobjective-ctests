//
//  ASPostCell.m
//  APITest
//
//  Created by Oleksii Skutarenko on 14.03.14.
//  Copyright (c) 2014 Alex Skutarenko. All rights reserved.
//

#import "ASPostCell.h"

@implementation ASPostCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//Считаем высоты ячейки(важно учитывть все размеры, указанные в сториборде)
+ (CGFloat) heightForText:(NSString*) text {
    //Отступ с 4 сторон по 5
    CGFloat offset = 5.0;
    //Размер шрифта
    UIFont* font = [UIFont systemFontOfSize:14.f];
    //Тень, ее радиус и отступ
    NSShadow* shadow = [[NSShadow alloc] init];
    shadow.shadowOffset = CGSizeMake(0, -1);
    shadow.shadowBlurRadius = 0.5;
    //При ориентации по центру(в данном случае учитывается перенос слов)
    NSMutableParagraphStyle* paragraph = [[NSMutableParagraphStyle alloc] init];
    [paragraph setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraph setAlignment:NSTextAlignmentCenter];
    //Добавляем атрибуды для строки
    NSDictionary* attributes =
    [NSDictionary dictionaryWithObjectsAndKeys:
     font, NSFontAttributeName,
     paragraph, NSParagraphStyleAttributeName,
     shadow, NSShadowAttributeName, nil];
    //Подсчет размеров ячейки(
    CGRect rect = [text boundingRectWithSize:CGSizeMake(320 - 2 * offset, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];
    
    //Возвращаем высоту ячейки
    return CGRectGetHeight(rect) + 2 * offset;
}

@end
