//
//  ViewController.m
//  AnimationHomeWork
//
//  Created by Admin on 07.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@property(strong, nonatomic) UIImageView* testView;
@property(strong, nonatomic) NSArray* arrayOfColors;
@property(strong, nonatomic) NSMutableArray* arrayOfSubView;
@property(strong, nonatomic) NSMutableArray* arrayOfCornerCenters;
@property(strong, nonatomic) NSMutableArray* arrayOfImages;
@property(assign, nonatomic) NSInteger indexSquare;
@property(assign, nonatomic) NSInteger indexCorner;


@end

@implementation ViewController


#pragma mark - LevelStudent

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayOfColors = [NSArray arrayWithObjects:[UIColor redColor], [UIColor greenColor],  [UIColor yellowColor],
                           [UIColor blueColor], nil];
    self.arrayOfSubView = [NSMutableArray array];
    self.arrayOfCornerCenters = [NSMutableArray array];
    self.arrayOfImages = [NSMutableArray array];
    self.indexSquare = 0;
    self.indexCorner = 0;
    
    //Создание квадратов в углах
    [self newSquare:self.arrayOfColors];
    [self.arrayOfSubView exchangeObjectAtIndex:2 withObjectAtIndex:3];
    
    //Замена цвета двух квадратов
    for (int i = 2; i < 4; i++) {
        UIImageView* view = [self.arrayOfSubView objectAtIndex:i];
        view.backgroundColor = [self.arrayOfColors objectAtIndex:i];
    }
    
    //Определение центров в углах
    [self cornerCenter];
    
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    for (int i = 0; i < [self.arrayOfSubView count]; i++) {
        
        UIImageView* view = [self.arrayOfSubView objectAtIndex:i];
        
        self.indexSquare = i;
        
        if (self.indexSquare == 3) {
            self.indexCorner = 0;
        } else {
            self.indexCorner = i + 1;
        }
        
        [self moveView:view atCorner:self.indexCorner];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - MyFuctions


-(void) newImage {
    
    UIImage* image1 = [UIImage imageNamed:@"1.png"];
    [self.arrayOfImages addObject:image1];

    UIImage* image2 = [UIImage imageNamed:@"2.png"];
    [self.arrayOfImages addObject:image2];
    [self.arrayOfImages addObject:image1];

    UIImage* image3 = [UIImage imageNamed:@"3.png"];
    [self.arrayOfImages addObject:image3];


}


-(void) moveView:(UIImageView*) view atCorner:(NSInteger) cornerIndex {
    
    [UIView animateWithDuration:4
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         //CGAffineTransform rotation = CGAffineTransformMakeRotation(M_PI_2);
                         
                         //view.transform = rotation;
                         
                         view.center = CGPointFromString([self.arrayOfCornerCenters objectAtIndex:cornerIndex]);
                         
                         view.backgroundColor = [self.arrayOfColors objectAtIndex:cornerIndex];
                         
                     }
                     completion:^(BOOL finished) {
                         
                         __weak UIImageView* newView = view;
                         
                         NSInteger newCorner = (cornerIndex == 3) ? 0 : (cornerIndex + 1);
                         
                         [self moveView:newView atCorner:newCorner];
                         
                     }];
}



-(void) cornerCenter {
    
    CGPoint cornerCenter0 = CGPointMake(CGRectGetMinX(self.view.bounds) + CGRectGetWidth(self.testView.bounds) / 2,
                                        CGRectGetMinY(self.view.bounds) + CGRectGetHeight(self.testView.bounds) / 2);
    [self.arrayOfCornerCenters addObject:NSStringFromCGPoint(cornerCenter0)];
    
    CGPoint cornerCenter1 = CGPointMake(CGRectGetMinX(self.view.bounds) + CGRectGetWidth(self.testView.bounds) / 2,
                                        CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.testView.bounds) / 2);
    [self.arrayOfCornerCenters addObject:NSStringFromCGPoint(cornerCenter1)];
    
    CGPoint cornerCenter2 = CGPointMake(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(self.testView.bounds) / 2,
                                        CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.testView.bounds) / 2);
    [self.arrayOfCornerCenters addObject:NSStringFromCGPoint(cornerCenter2)];
    
    CGPoint cornerCenter3 = CGPointMake(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(self.testView.bounds) / 2,
                                        CGRectGetMinY(self.view.bounds) + CGRectGetHeight(self.testView.bounds) / 2);
    [self.arrayOfCornerCenters addObject:NSStringFromCGPoint(cornerCenter3)];
}


-(void) newSquare:(NSArray*) array {
    
    NSInteger count = 0;
    
    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 2; j++) {
            
            CGRect square = self.view.bounds;
            square.size = CGSizeMake(square.size.width / 4, square.size.width / 4);
            square.origin.x = 0 + (CGRectGetWidth(self.view.bounds) - square.size.width) * i;
            square.origin.y = 0 + (CGRectGetHeight(self.view.bounds) - square.size.height) * j;
            
            self.testView = [[UIImageView alloc] initWithFrame:square];
            
            self.testView.backgroundColor = [array objectAtIndex:count];
            
            [self newImage];
            
            self.testView.animationImages = self.arrayOfImages;
            self.testView.animationDuration = 1.f;
            [self.testView startAnimating];
            
            [self.view addSubview:self.testView];
            
            [self.arrayOfSubView addObject:self.testView];
            
            count++;
        }
    }
}




//Ученик
/*
- (void)viewDidLoad {
    [super viewDidLoad];
 
    for (int i = 0; i < 4; i++) {
 
        self.testView = [[UIView alloc] initWithFrame:[self square:i]];
        self.testView.backgroundColor = [UIColor redColor];
 
        [self.view addSubview:self.testView];
    }
}


-(void) viewDidAppear:(BOOL)animated {
 
    [super viewDidAppear:animated];
 
    NSInteger i = 0;
 
    for (UIView* view in self.view.subviews) {
 
        [self moveView:view andCurve:i << 16];
 
        i++;
    }
 
 
 
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) moveView:(UIView*) newView andCurve:(UIViewAnimationOptions) curve {
    
    [UIView animateWithDuration:4
                          delay:0
                        options:curve | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         
                         //newView.center = CGPointMake(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(newView.bounds) / 2, CGRectGetMinY(newView.bounds));
                         
                         CGAffineTransform rotation = CGAffineTransformMakeTranslation(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(newView.bounds), 0);
                         
                         newView.backgroundColor = [self randomColor];
                         
                         newView.transform = rotation;
                         
                     }
                     completion:^(BOOL finished) {
                         
                         NSLog(@"Animation finished!");
                         
                         __weak UIView* v = newView;
                         
                         [self moveView:v andCurve:curve];
                     }];
}


- (UIColor*) randomColor {
    CGFloat r = (CGFloat)(arc4random() % 256) / 255.f;
    CGFloat g = (CGFloat)(arc4random() % 256) / 255.f;
    CGFloat b = (CGFloat)(arc4random() % 256) / 255.f;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}


-(CGRect) square:(NSInteger) i {
    
    CGRect square = self.view.bounds;
    square.origin = CGPointMake(0, square.size.height / 8 + (square.size.height / 8) * 2 * i);
    square.size = CGSizeMake(square.size.width / 8, square.size.width / 8);
    
    return square;
}
*/

@end
