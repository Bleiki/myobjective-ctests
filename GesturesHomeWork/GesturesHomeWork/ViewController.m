//
//  ViewController.m
//  GesturesHomeWork
//
//  Created by Admin on 11.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(weak, nonatomic) UIImageView* testView;
@property(strong, nonatomic) NSMutableArray* arrayOfImages;

@end

@implementation ViewController

#pragma mark - StandartFunctions

- (void)viewDidLoad {
    [super viewDidLoad];
    //Created view.
    self.arrayOfImages = [NSMutableArray array];
    
    CGRect square = CGRectMake(CGRectGetMidX(self.view.bounds) - 25,
                               CGRectGetMidY(self.view.bounds) - 25,
                               50,
                               50);
    
    UIImageView* view = [[UIImageView alloc] initWithFrame:square];
    
    view.backgroundColor = [UIColor greenColor];
    
    self.testView = view;
    
    [self.view addSubview:self.testView];
    
    //Created animation images on view.
    UIImage* image1 = [UIImage imageNamed:@"1.png"];
    [self.arrayOfImages addObject:image1];
    
    UIImage* image2 = [UIImage imageNamed:@"2.png"];
    [self.arrayOfImages addObject:image2];
    [self.arrayOfImages addObject:image1];
    
    UIImage* image3 = [UIImage imageNamed:@"3.png"];
    [self.arrayOfImages addObject:image3];
    
    self.testView.animationImages = self.arrayOfImages;
    self.testView.animationDuration = 1.f;
    [self.testView startAnimating];
    
    //Created TapGestures.
    UITapGestureRecognizer* oneTapGestures = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handTap:)];
    
    [self.view addGestureRecognizer:oneTapGestures];
    
    //Created DoubleTapGestures.
    UITapGestureRecognizer* doubleTapGestures = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handDoubleTap:)];
    
    doubleTapGestures.numberOfTapsRequired = 2;
    
    [self.view addGestureRecognizer:doubleTapGestures];
    
    [oneTapGestures requireGestureRecognizerToFail:doubleTapGestures];
    
    //Created RightSwipeGestures.
    UISwipeGestureRecognizer* rightSwipeGuesters = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handRightSwipe:)];
    
    rightSwipeGuesters.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:rightSwipeGuesters];
    
    //Created LeftSwipeGestures.
    UISwipeGestureRecognizer* leftSwipeGuesters = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handLeftSwipe:)];
    
    rightSwipeGuesters.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [self.view addGestureRecognizer:leftSwipeGuesters];
    
    //Created PinchGestures.
    UIPinchGestureRecognizer* pinchGuesturs = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handPinch:)];
    
    [self.view addGestureRecognizer:pinchGuesturs];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - MyMethods

-(void) handTap:(UITapGestureRecognizer*) oneTapGesturs {
    
    NSLog(@"OneTap");
    
    [UIView animateWithDuration:2
                     animations:^{
                         CGPoint tap = [oneTapGesturs locationInView:self.view];
                         
                         self.testView.center = tap;
                     }];
}

-(void) handDoubleTap:(UITapGestureRecognizer*) doubleTapGesturs {
    
    NSLog(@"DoubleTap");
    
    [self.testView.layer removeAllAnimations];
}

-(void) handPinch:(UIPinchGestureRecognizer*) pinchGuesters {
    
    
}

-(void) handRightSwipe:(UISwipeGestureRecognizer*) rightSwipeGuesters {
    
    [self rotation:M_PI withName:@"Right swipe"];
}

-(void) handLeftSwipe:(UISwipeGestureRecognizer*) leftSwipeGuesters {
    
    [self rotation:-3.14 withName:@"Left swipe"];
}

-(void) rotation:(CGFloat) angle withName:(NSString*) string {
    
    NSLog(@"%@", string);
    
    [UIView animateWithDuration:2
                     animations:^{
                         CGAffineTransform currentRotation = self.testView.transform;
                         
                         CGAffineTransform rotation = CGAffineTransformRotate(currentRotation, angle);
                         
                         self.testView.transform = rotation;
                     }];
}
@end
