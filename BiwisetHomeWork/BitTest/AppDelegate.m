//
//  AppDelegate.m
//  BitTest
//
//  Created by Admin on 03.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "AppDelegate.h"
#import "VGStudent.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
   
    /*
    NSMutableArray* arrayOfStudents = [NSMutableArray array];
    
    for (NSInteger i = 0; i < 10; i++) {
        
        VGStudent* student = [[VGStudent alloc] init];
        
        [arrayOfStudents addObject:student];
        
        student.subjectType = 1 << arc4random() %7 | 1 << arc4random() %7 | 1 << arc4random() %7;
        
        //NSLog(@"%@", [student description]);
    }
 
    NSInteger countOfDevelopment = 0;
    
    NSMutableArray* humanitaries = [NSMutableArray array];
    
    NSMutableArray* tehnicals = [NSMutableArray array];

    
    for (VGStudent* student in arrayOfStudents) {
        
        if ((student.subjectType & VGStudentSubjectTypeMath) |
            (student.subjectType & VGStudentSubjectTypeDevelopment) |
            (student.subjectType & VGStudentSubjectTypeEngineering)) {
            
            [tehnicals addObject:student];
            
            if (student.subjectType & VGStudentSubjectTypeDevelopment) {
                countOfDevelopment++;
            }
        } else if ((student.subjectType & VGStudentSubjectTypeBiologi) |
                   (student.subjectType & VGStudentSubjectTypeArt) |
                   (student.subjectType & VGStudentSubjectTypeAnatomy) |
                   (student.subjectType & VGStudentSubjectTypePhycology)) {
            
            [humanitaries addObject:student];
        }
    }
    
    NSLog(@"%li", countOfDevelopment);
    NSLog(@"Tehnicals: %li", [tehnicals count]);
    NSLog(@"Humanitaries: %li", [humanitaries count]);

    for (VGStudent* student in arrayOfStudents) {
        
        if (student.subjectType & VGStudentSubjectTypeBiologi) {
            
            NSLog(@"%@", [student description]);
            
            student.subjectType = student.subjectType & ~VGStudentSubjectTypeBiologi;
            
            NSLog(@"Biologi CANCELED!!!");
            NSLog(@"%@", [student description]);
        }
    }
     */
    //NSLog(@"%li", NSIntegerMax);
    NSInteger randomNumber = arc4random();
    NSInteger number = randomNumber;
    NSInteger theBalance = 0;
    NSMutableString* bitNumber = [NSMutableString string];
    
    while (number != 0) {
        
        if (!(bitNumber.length % 5)) {
            
            [bitNumber insertString:[NSString stringWithFormat:@" "] atIndex:0];
        }
        
        theBalance = number % 2;
        
        number = number / 2;
        
        [bitNumber insertString:[NSString stringWithFormat:@"%li", theBalance] atIndex:0];
    }
    
    NSLog(@"%li = %@", randomNumber, bitNumber);
    
    return YES;
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
