//
//  VGStudent.m
//  BitTest
//
//  Created by Admin on 03.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "VGStudent.h"

@implementation VGStudent

-(NSString*) answerType:(VGStudentSubjectType) type {
    return self.subjectType & type ? @"YES" : @"NO";
}

-(NSString*) description {
    
    return [NSString stringWithFormat:@"Student studies:\n"
            "biology = %@\n"
            "math = %@\n"
            "development = %@\n"
            "engineering = %@\n"
            "art = %@\n"
            "phycology = %@\n"
            "anbatomy = %@\n",
            [self answerType:VGStudentSubjectTypeBiologi],
            [self answerType:VGStudentSubjectTypeMath],
            [self answerType:VGStudentSubjectTypeDevelopment],
            [self answerType:VGStudentSubjectTypeEngineering],
            [self answerType:VGStudentSubjectTypeArt],
            [self answerType:VGStudentSubjectTypePhycology],
            [self answerType:VGStudentSubjectTypeAnatomy]];
}

@end
