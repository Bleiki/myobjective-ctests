//
//  VGStudent.h
//  BitTest
//
//  Created by Admin on 03.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    VGStudentSubjectTypeBiologi         = 1 << 0,
    VGStudentSubjectTypeMath            = 1 << 1,
    VGStudentSubjectTypeDevelopment     = 1 << 2,
    VGStudentSubjectTypeEngineering     = 1 << 3,
    VGStudentSubjectTypeArt             = 1 << 4,
    VGStudentSubjectTypePhycology       = 1 << 5,
    VGStudentSubjectTypeAnatomy         = 1 << 6
    
}VGStudentSubjectType;

@interface VGStudent : NSObject

@property(assign, nonatomic) VGStudentSubjectType subjectType;
@property(strong, nonatomic) NSString* name;

-(NSString*) answerType:(VGStudentSubjectType) type;
-(NSString*) description;


@end
