//
//  ViewController.h
//  DrawingHomeWork
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VGDrawingView;

@interface ViewController : UIViewController

@property(weak, nonatomic) IBOutlet VGDrawingView* drawingView;

@end

