//
//  VGDrawingView.m
//  DrawingHomeWork
//
//  Created by Admin on 17.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "VGDrawingView.h"

@implementation VGDrawingView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    //[super drawRect:rect];
    
    NSLog(@"drawRect %@", NSStringFromCGRect(rect));
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //Square
    
    /*CGRect square = CGRectMake(0,
                               (MAX(CGRectGetWidth(rect), CGRectGetHeight(rect)) - MIN(CGRectGetWidth(rect), CGRectGetHeight(rect))) / 2,
                               MIN(CGRectGetWidth(rect), CGRectGetHeight(rect)),
                               MIN(CGRectGetWidth(rect), CGRectGetHeight(rect)));*/
    
    CGContextSetStrokeColorWithColor(context, [UIColor whiteColor].CGColor);
    
    CGRect square = CGRectMake(60, 100, 200, 200);
    
    
    CGContextAddRect(context, square);
    
    CGContextStrokePath(context);
    
    //Points on square
    CGPoint topPoint = CGPointMake(CGRectGetMidX(square), CGRectGetMinY(square));
    CGPoint leftPoint = CGPointMake(CGRectGetMinX(square), CGRectGetMinY(square) + CGRectGetMaxY(square) / 4);
    CGPoint rightPoint = CGPointMake(CGRectGetMaxX(square), CGRectGetMinY(square) + CGRectGetMaxY(square) / 4);
    CGPoint leftBotPoint = CGPointMake(CGRectGetMinX(square) + CGRectGetWidth(square) / 6, CGRectGetMaxY(square));
    CGPoint rightBotPoint = CGPointMake(CGRectGetMinX(square) + (CGRectGetWidth(square) / 6) * 5, CGRectGetMaxY(square));

    NSLog(@"%@, %@, %@, %@, %@", NSStringFromCGPoint(topPoint), NSStringFromCGPoint(leftPoint),
             NSStringFromCGPoint(rightPoint), NSStringFromCGPoint(leftBotPoint), NSStringFromCGPoint(rightBotPoint));
    
    //Lines
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
    CGContextSetLineWidth(context, 3);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    CGContextMoveToPoint(context, topPoint.x, topPoint.y);
    CGContextAddLineToPoint(context, leftBotPoint.x, leftBotPoint.y);
    
    CGContextMoveToPoint(context, topPoint.x, topPoint.y);
    CGContextAddLineToPoint(context, rightBotPoint.x, rightBotPoint.y);
    
    CGContextMoveToPoint(context, leftPoint.x, leftPoint.y);
    CGContextAddLineToPoint(context, rightPoint.x, rightPoint.y);
    
    CGContextMoveToPoint(context, leftBotPoint.x, leftBotPoint.y);
    CGContextAddLineToPoint(context, rightPoint.x, rightPoint.y);
    
    CGContextMoveToPoint(context, rightBotPoint.x, rightBotPoint.y);
    CGContextAddLineToPoint(context, leftPoint.x, leftPoint.y);
    
    CGContextStrokePath(context);
    
    //Circles
    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    
    CGContextAddEllipseInRect(context, CGRectMake(topPoint.x - 25, topPoint.y - 25, 50, 50));
    CGContextAddEllipseInRect(context, CGRectMake(leftPoint.x - 25, leftPoint.y - 25, 50, 50));
    CGContextAddEllipseInRect(context, CGRectMake(rightPoint.x - 25, rightPoint.y - 25, 50, 50));
    CGContextAddEllipseInRect(context, CGRectMake(leftBotPoint.x - 25, leftBotPoint.y - 25, 50, 50));
    CGContextAddEllipseInRect(context, CGRectMake(rightBotPoint.x - 25, rightBotPoint.y - 25, 50, 50));
    
    CGContextStrokePath(context);

    

}


@end
