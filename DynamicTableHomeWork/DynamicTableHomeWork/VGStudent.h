//
//  VGStudent.h
//  DynamicTableHomeWork
//
//  Created by Admin on 30.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VGStudent : NSObject

@property (strong, nonatomic) NSString* name;
@property (assign, nonatomic) NSInteger averageMark;
@property (strong, nonatomic) UIColor * color;



@end
