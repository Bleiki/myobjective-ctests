//
//  VGColorAndName.h
//  DynamicTableHomeWork
//
//  Created by Admin on 30.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface VGColorAndName : NSObject

@property (strong, nonatomic) UIColor* color;
@property (strong, nonatomic) NSString* name;

@end
