//
//  ViewController.m
//  DynamicTableHomeWork
//
//  Created by Admin on 30.12.16.
//  Copyright © 2016 Vyacheslav Gerasimenok. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (assign, nonatomic) CGFloat redColor;
@property (assign, nonatomic) CGFloat greenColor;
@property (assign, nonatomic) CGFloat blueColor;

@property (strong, nonatomic) NSMutableArray* arrayOfColors;
@property (strong, nonatomic) NSMutableArray* arrayOfStudents;
@property (strong, nonatomic) NSMutableArray* arrayOfDvoechnik;
@property (strong, nonatomic) NSMutableArray* arrayOfTroechnik;
@property (strong, nonatomic) NSMutableArray* arrayOfHoroshist;
@property (strong, nonatomic) NSMutableArray* arrayOfOtlichnik;

@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIEdgeInsets inset = UIEdgeInsetsMake(20, 0, 0, 0);
    self.tableView.contentInset = inset;
    self.tableView.scrollIndicatorInsets = inset;
    
    self.arrayOfColors = [NSMutableArray array];
    self.arrayOfStudents = [NSMutableArray array];
    self.arrayOfDvoechnik = [NSMutableArray array];
    self.arrayOfTroechnik = [NSMutableArray array];
    self.arrayOfHoroshist = [NSMutableArray array];
    self.arrayOfOtlichnik = [NSMutableArray array];
    
    NSArray* arrayOfNames = [NSArray arrayWithObjects:@"Viacheslav", @"Viktoriya", @"Vladimir", @"Olga", @"Irina", @"Elena", @"Svetlana", @"Alexandr", @"Petr", @"Fedor", @"Konstantin", @"Igor", @"Mark", @"Oleg", @"Dmitrii", @"Evgeniy", @"Viacheslav", @"Vsevolod", @"Galina", @"Sergey",nil];
    
    for (NSInteger i = 0; i < 10; i++) {
        
        VGStudent* student = [[VGStudent alloc] init];
        
        student.color = [self randomColor];
        
        student.name = [NSString stringWithFormat:@"RGB (%i, %i, %i)", (int)(self.redColor * 255), (int)(self.greenColor * 255), (int)(self.blueColor * 255)];
        
        [self.arrayOfColors addObject:student];
    }
    
    for (NSInteger i = 0; i < 20; i++) {
        
        VGStudent* student = [[VGStudent alloc] init];
        
        student.averageMark = arc4random() % 4 + 2;
        
        student.name = [arrayOfNames objectAtIndex:i];
        
        [self.arrayOfStudents addObject:student];
    }
    
    for (VGStudent* student in self.arrayOfStudents) {
        
        switch (student.averageMark) {
            case 2:
                [self.arrayOfDvoechnik addObject:student];
                break;
                
            case 3:
                [self.arrayOfTroechnik addObject:student];
                break;
                
            case 4:
                [self.arrayOfHoroshist addObject:student];
                break;
                
            case 5:
                [self.arrayOfOtlichnik addObject:student];
                break;
                
            default:
                break;
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 5;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    NSString* header;

    switch (section) {
        case 0:
            header = @"Dvoechniki";
            break;
            
        case 1:
            header = @"Troechniki";
            break;
            
        case 2:
            header = @"Horoshisti";
            break;
            
        case 3:
            header = @"Otlichniki";
            break;
            
        case 4:
            header = @"Colors";
            break;
            
        default:
            break;
    }
    
    return header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger numberOfRows;
    
    switch (section) {
        case 0:
            numberOfRows = [self.arrayOfDvoechnik count];
            break;
            
        case 1:
            numberOfRows = [self.arrayOfTroechnik count];
            break;
            
        case 2:
            numberOfRows = [self.arrayOfHoroshist count];
            break;
            
        case 3:
            numberOfRows = [self.arrayOfOtlichnik count];
            break;
            
        case 4:
            numberOfRows = [self.arrayOfColors count];
            break;
            
        default:
            break;
    }
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString* identifierForColor = @"CellWithColor";
    static NSString* identifierForStudent = @"CellWithStudents";
    
    if (indexPath.section == 4) {
        
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifierForColor];
        
        if (!cell) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifierForColor];
        }
        
        VGStudent* StudentWithColor = [self.arrayOfColors objectAtIndex:indexPath.row];
        
        cell.backgroundColor = StudentWithColor.color;
        
        cell.textLabel.text = StudentWithColor.name;
        
        return cell;
        
    } else {
    
        UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifierForStudent];
    
        if (!cell) {
        
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifierForStudent];
        }
    
        NSMutableArray* currentArray;
    
        switch (indexPath.section) {
            case 0:
                currentArray = self.arrayOfDvoechnik;
                break;
            
            case 1:
                currentArray = self.arrayOfTroechnik;
                break;
            
            case 2:
                currentArray = self.arrayOfHoroshist;
                break;
            
            case 3:
                currentArray = self.arrayOfOtlichnik;
                break;
                
            default:
                break;
        }
    
        NSArray* sortedArrayOfStudents = [currentArray sortedArrayUsingComparator:^NSComparisonResult(VGStudent* obj1, VGStudent* obj2) {
            return [obj1.name compare:obj2.name];
        }];
    
        VGStudent* student = [sortedArrayOfStudents objectAtIndex:indexPath.row];

        cell.textLabel.text = student.name;
    
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%li", student.averageMark ];
     
        return cell;
    }
}


#pragma mark - My Methods

-(UIColor*) randomColor {
    
    self.redColor = (CGFloat)(arc4random() % 256) / 255;
    self.greenColor = (CGFloat)(arc4random() % 256) / 255;
    self.blueColor = (CGFloat)(arc4random() % 256) / 255;

    UIColor* randomColor = [UIColor colorWithRed:self.redColor green:self.greenColor blue:self.blueColor alpha:1];
    
    return randomColor;
}




@end
